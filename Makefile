.PHONY: migrate
migrate:
	php artisan migrate:fresh
	php artisan db:seed --class=InstallSeeder
	php artisan db:seed --class=TestSeeder

ide:
	php artisan ide-helper:generate
	php artisan ide-helper:model -M
	php artisan ide-helper:meta

push:
	php vendor/bin/pint
	php vendor/bin/rector process
	php artisan test
	php vendor/bin/phpstan analyse
	git push origin develop

deploy:
	php vendor/bin/dep deploy -o ssh_multiplexing=false

changelog:
	php artisan changelog:add
