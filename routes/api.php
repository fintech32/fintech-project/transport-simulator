<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', fn (Request $request) => $request->user());

Route::prefix('core')->group(function () {
    Route::prefix('hub')->group(function () {
        Route::get('/', (new \App\Http\Controllers\Api\Core\HubController())->index(...));
        Route::get('info', (new \App\Http\Controllers\Api\Core\HubController())->info(...));
    });

    Route::prefix('ligne')->group(function () {
        Route::get('info/{ligne_id}', (new \App\Http\Controllers\Api\Core\LigneController())->info(...));
    });
});

Route::prefix('compta')->group(function () {
    Route::post('resultat_chart', [\App\Http\Controllers\Api\Compta\ComptaController::class, 'resultat_chart']);
});
