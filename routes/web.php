<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('auth:web')->group(function () {
    Route::get('/', \App\Http\Controllers\HomeController::class)->name('home');

    Route::prefix('hub')->name('hub.')->group(function () {
        Route::get('/', (new \App\Http\Controllers\Hub\HubController())->index(...))->name('index');
        Route::get('/create', (new \App\Http\Controllers\Hub\HubController())->create(...))->name('create');
        Route::post('/create', (new \App\Http\Controllers\Hub\HubController())->store(...))->name('store');
        Route::get('{hub_id}', (new \App\Http\Controllers\Hub\HubController())->show(...))->name('show');

        Route::prefix('{hub_id}/ligne')->name('ligne.')->group(function () {
            Route::get('/', (new \App\Http\Controllers\Hub\LigneController())->index(...))->name('index');
            Route::get('create', (new \App\Http\Controllers\Hub\LigneController())->create(...))->name('create');
            Route::post('create', (new \App\Http\Controllers\Hub\LigneController())->store(...))->name('store');
            Route::get('{ligne_id}', (new \App\Http\Controllers\Hub\LigneController())->show(...))->name('show');
        });
    });

    Route::prefix('shop')->name('shop.')->group(function () {
        Route::get('/', (new \App\Http\Controllers\Shop\ShopController())->index(...))->name('index');
        Route::get('checkout', (new \App\Http\Controllers\Shop\ShopController())->checkout(...))->name('checkout');
    });
});

Route::get('/test', (new \App\Http\Controllers\CoreController())->test(...));
Route::get('/register/config', (new \App\Http\Controllers\CoreController())->config(...))->name('register.config');
Route::post('/register/config', (new \App\Http\Controllers\CoreController())->store(...))->name('register.config.store');

Route::get('/roadmap', (new \App\Http\Controllers\CoreController())->roadmap(...))->name('roadmap');
