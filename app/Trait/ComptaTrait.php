<?php

namespace App\Trait;

use App\Models\User;
use App\Models\Users\UserCompany;
use Carbon\Carbon;

class ComptaTrait
{
    public static function createDebit(User $user, string $title, float|string $amount, string $type_account, string $type, bool $valorisation = true): void
    {
        // Nouveau Mouvement
        $user->company->mouvements()->create([
            'title' => $title,
            'amount' => -floatval($amount),
            'type_account' => $type_account,
            'type_mvm' => $type,
            'user_company_id' => $user->company->id,
        ]);

        $user->update([
            'argent' => $user->argent - floatval($amount),
        ]);

        if ($valorisation) {
            $user->company()->update([
                'valorisation' => $user->company->valorisation + floatval($amount),
            ]);
        }
    }

    public static function createCredit(User $user, string $title, float|string $amount, string $type_account, string $type, bool $valorisation = true): void
    {
        // Nouveau Mouvement
        $user->company->mouvements()->create([
            'title' => $title,
            'amount' => floatval($amount),
            'type_account' => $type_account,
            'type_mvm' => $type,
            'user_company_id' => $user->company->id,
        ]);

        $user->update([
            'argent' => $user->argent + floatval($amount),
        ]);

        if ($valorisation) {
            $user->company()->update([
                'valorisation' => $user->company->valorisation + floatval($amount),
            ]);
        }
    }

    public static function totalProduit(UserCompany $company)
    {
        return $company->mouvements()->where('type_account', 'produit')->get()->sum('amount');
    }

    public static function totalProduitForHub(UserCompany $company, $user_hub_id)
    {
        return $company->mouvements()
            ->where('type_account', 'produit')
            ->where('user_hub_id', $user_hub_id)
            ->get()
            ->sum('amount');
    }

    public static function totalCharge(UserCompany $company)
    {
        return $company->mouvements()->where('type_account', 'charge')->get()->sum('amount');
    }

    public static function totalChargeForHub(UserCompany $company, $user_hub_id)
    {
        return $company->mouvements()
            ->where('type_account', 'charge')
            ->where('user_hub_id', $user_hub_id)
            ->get()
            ->sum('amount');
    }

    public static function calcCAJournalier(Carbon $date, $user_hub_id)
    {
        return auth()->user()->company->mouvements()
            ->where('user_hub_id', $user_hub_id)
            ->where('type_account', 'produit')
            ->whereBetween('created_at', [$date->startOfDay(), $date->endOfDay()])
            ->get()
            ->sum('amount');
    }

    public static function calcBeneficeJournalier(Carbon $date, $user_hub_id)
    {
        $produit = auth()->user()->company->mouvements()
            ->where('user_hub_id', $user_hub_id)
            ->where('type_account', 'produit')
            ->whereBetween('created_at', [$date->startOfDay(), $date->endOfDay()])
            ->get()
            ->sum('amount');

        $charge = auth()->user()->company->mouvements()
            ->where('user_hub_id', $user_hub_id)
            ->where('type_account', 'charge')
            ->whereBetween('created_at', [$date->startOfDay(), $date->endOfDay()])
            ->get()
            ->sum('amount');

        return $produit - $charge;
    }
}
