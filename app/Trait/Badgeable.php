<?php

namespace App\Trait;

use App\Models\Core\Badge;

trait Badgeable
{
    public function badges()
    {
        return $this->belongsToMany(Badge::class);
    }
}
