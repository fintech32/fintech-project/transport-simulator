<?php

namespace App\Trait;

trait CompanyTrait
{
    public static function calcSubvention($tresorerie_structurel)
    {
        return match ($tresorerie_structurel) {
            $tresorerie_structurel <= 12000 => 10,
            $tresorerie_structurel <= 29000 => 23,
            $tresorerie_structurel <= 42000 => 32,
            $tresorerie_structurel <= 60000 => 41,
            $tresorerie_structurel <= 79000 => 56,
            $tresorerie_structurel <= 90000 => 68,
            $tresorerie_structurel <= 150000 => 80,
            default => 85
        };
    }
}
