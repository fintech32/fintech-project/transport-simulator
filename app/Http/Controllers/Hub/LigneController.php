<?php

namespace App\Http\Controllers\Hub;

use App\Http\Controllers\Controller;
use App\Models\Core\Ligne;
use App\Models\Users\UserHub;
use App\Models\Users\UserLigne;
use App\Trait\ComptaTrait;
use Bavix\Wallet\Internal\Exceptions\ExceptionInterface;
use Illuminate\Http\Request;

class LigneController extends Controller
{
    public function index($hub_id)
    {
        $lignes = UserHub::find($hub_id)->lignes;

        return view('hub.ligne.index', [
            'hub' => UserHub::find($hub_id),
            'lignes' => $lignes,
        ]);
    }

    public function create($hub_id)
    {
        $hub = UserHub::find($hub_id);

        return view('hub.ligne.create', [
            'hub' => $hub,
            'lignes' => Ligne::where('hub_id', $hub->hub_id)->get(),
        ]);
    }

    /**
     * @throws ExceptionInterface
     */
    public function store($hub_id, Request $request)
    {
        $hub = UserHub::find($hub_id);
        $l = Ligne::find($request->get('ligne_id'));
        $nb_quai = $hub->hub->nb_quai;
        $hub->lignes()->create([
            'date_achat' => now(),
            'nb_depart_jour' => intval(config('app.system.nb_min_in_day') / ($l->time_min * 2 + 15)), /** @phpstan-ignore-line */
            'quai' => random_int(1, $nb_quai),
            'user_hub_id' => $hub->id,
            'ligne_id' => $request->get('ligne_id'),
        ]);

        ComptaTrait::createDebit(
            $hub->user,
            'Achat ligne: '.$l->station_start.' <=> '.$l->station_end,
            $l->price,
            'charge',
            'achat_ligne'
        );

        return redirect()->back()->with('success', "La ligne $l->station_start <=> $l->station_end à été acheter avec succès");
    }

    public function show($hub_id, $ligne_id)
    {
        return view('hub.ligne.show', [
            'hub' => UserHub::find($hub_id),
            'ligne' => UserLigne::find($ligne_id),
        ]);
    }
}
