<?php

namespace App\Http\Controllers\Hub;

use App\Http\Controllers\Controller;
use App\Models\Core\Hub;
use App\Models\Users\UserHub;
use App\Trait\ComptaTrait;
use Illuminate\Http\Request;

class HubController extends Controller
{
    public function index()
    {
        $hubs = auth()->user()->hubs;

        return view('hub.index', ['hubs' => $hubs]);
    }

    public function create()
    {
        return view('hub.create', [
            'hubs' => Hub::all(),
        ]);
    }

    public function store(Request $request)
    {
        $hub = Hub::find($request->get('hub_id'));

        auth()->user()->hubs()->create([
            'nb_salarie_iv' => intval($hub->superficie_infra / 1000 * 1),
            'nb_salarie_ir' => intval($hub->superficie_infra / 1000 * 2),
            'nb_salarie_com' => intval($hub->superficie_infra / 1000 * 2),
            'nb_salarie_technicentre' => 0,
            'date_achat' => now(),
            'km_ligne' => 0,
            'user_id' => auth()->user()->id,
            'hub_id' => $hub->id,
        ]);

        // Active les recherches du HUB

        ComptaTrait::createDebit(
            $hub->users,
            'Achat HUB:'.$hub->name,
            $hub->price,
            'charge',
            'achat_hub',
        );
        ComptaTrait::createCredit(
            $hub->users,
            "Subvention Achat d'un HUB",
            $request->get('price_subvention'),
            'produit',
            'subvention'
        );

        return redirect()->back()->with('success', "Le hub $hub->name à été acheter avec succès");
    }

    public function show($hub_id)
    {
        $hub = UserHub::find($hub_id);

        return view('hub.show', [
            'hub' => $hub,
        ]);
    }
}
