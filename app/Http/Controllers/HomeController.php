<?php

namespace App\Http\Controllers;

use App\Charts\ChartEvolResultat;
use App\Models\Users\UserCompany;

class HomeController extends Controller
{
    public function __invoke()
    {
        $chartEvolResultat = new ChartEvolResultat();
        $chartEvolResultat->options([
            'width' => '100px',
        ]);
        $chartEvolResultat->labels([
            \Str::ucfirst(now()->subMonths(3)->endOfMonth()->translatedFormat('F')),
            \Str::ucfirst(now()->subMonths(2)->endOfMonth()->translatedFormat('F')),
            \Str::ucfirst(now()->subMonths(1)->endOfMonth()->translatedFormat('F')),
            \Str::ucfirst(now()->endOfMonth()->translatedFormat('F')),
        ]);
        $chartEvolResultat->type('line');
        $chartEvolResultat->dataset('Trésorerie Structurelle', 'line', [
            UserCompany::find(auth()->user()->company->id)->situation()->where('date', now()->subMonths(3)
                ->endOfMonth()->format('Y-m-d'))->first()->structural_cash ?? 0,
            UserCompany::find(auth()->user()->company->id)->situation()->where('date', now()->subMonths(2)
                ->endOfMonth()->format('Y-m-d'))->first()->structural_cash ?? 0,
            UserCompany::find(auth()->user()->company->id)->situation()->where('date', now()->subMonths()
                ->endOfMonth()->format('Y-m-d'))->first()->structural_cash ?? 0,
            UserCompany::find(auth()->user()->company->id)->situation()->where('date', now()
                ->endOfMonth()->format('Y-m-d'))->first()->structural_cash ?? 0,
        ])->options([
            'borderColor' => '#14A006',
        ]);
        $chartEvolResultat->dataset('Bénéfice Général', 'line', [
            UserCompany::find(auth()->user()->company->id)->situation()->where('date', now()->subMonths(3)
                ->endOfMonth()->format('Y-m-d'))->first()->benefice ?? 0,
            UserCompany::find(auth()->user()->company->id)->situation()->where('date', now()->subMonths(2)
                ->endOfMonth()->format('Y-m-d'))->first()->benefice ?? 0,
            UserCompany::find(auth()->user()->company->id)->situation()->where('date', now()->subMonths()
                ->endOfMonth()->format('Y-m-d'))->first()->benefice ?? 0,
            UserCompany::find(auth()->user()->company->id)->situation()->where('date', now()
                ->endOfMonth()->format('Y-m-d'))->first()->benefice ?? 0,
        ])->options([
            'borderColor' => '#0645A0',
        ]);

        return view('home', compact('chartEvolResultat'));
    }
}
