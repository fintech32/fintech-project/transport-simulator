<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Notifications\User\WelcomeNotification;
use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CoreController extends Controller
{
    public function test(): never
    {
        dd('OK');
    }

    public function config()
    {
        return view('auth.config', [
            'user' => auth()->user(),
            'faker' => Factory::create('fr_FR'),
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name_company' => 'required|string',
            'name_secretary' => 'required|string',
        ]);

        auth()->user()->update([
            'name_company' => $request->get('name_company'),
            'name_secretary' => $request->get('name_secretary'),
        ]);

        $company = auth()->user()->company()->create([
            'user_id' => auth()->user()->id,
            'general_rank' => User::count(),
            'distraction' => 0,
            'tarification' => 0,
            'ponctualite' => 0,
            'securite' => 0,
            'confort' => 0,
            'rent_aux' => 0,
            'frais' => 0,
        ]);
        $company->situation()->create([
            'date' => now()->subMonth()->endOfMonth(),
            'user_company_id' => $company->id,
        ]);

        if ($request->hasFile('logo_company')) {
            $name_company = Str::snake($request->get('name_company'));
            $request->file('logo_company')->storeAs(
                '/storages/avatar/company/',
                $name_company.'.'.$request->file('logo_company')->extension()
            );
            auth()->user()->update([
                'logo_company' => $name_company.'.'.$request->file('logo_company')->extension(),
            ]);
        }

        if ($request->hasFile('avatar_secretary')) {
            $name_secretay = Str::snake($request->get('name_secretary'));
            $request->file('avatar_secretary')->storeAs(
                '/storages/avatar/secretary/',
                $name_secretay.'.'.$request->file('avatar_secretary')->extension()
            );
            auth()->user()->update([
                'avatar_secretary' => $name_secretay.'.'.$request->file('avatar_secretary')->extension(),
            ]);
        }
        auth()->user()->notify(new WelcomeNotification(auth()->user()));

        return redirect()->route('home')->with('success', 'Bienvenue '.auth()->user()->name);
    }

    public function roadmap()
    {

    }
}
