<?php

namespace App\Http\Controllers\Shop;

use App\Events\PremiumEvent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index()
    {
        return view('shop.index', []);
    }

    public function checkout(Request $request)
    {
        return match ($request->get('event')) { /** @phpstan-ignore-line */
            'premium' => $this->checkoutPremium(),
            'tpoint-one' => $this->tpointOne()
        };
    }

    private function checkoutPremium()
    {
        $user = auth()->user();

        $user->update([
            'premium' => true,
        ]);

        event(new PremiumEvent($user));

        return redirect()->route('shop.index')->with('success', "Votre abonnement au compte Premium est désormais actif jusqu'au <strong>".now()->addMonth()->format('d/m/Y').'</strong>');
    }

    public function tpointOne()
    {
        $user = auth()->user();
        $user->update([
            'tpoint' => $user->tpoint + 100,
        ]);

        return redirect()->route('shop.index')->with('success', 'Votre achat à bien été pris en compte.');
    }
}
