<?php

namespace App\Http\Controllers\Api\Core;

use App\Http\Controllers\Api\ApiController;
use App\Models\Core\Ligne;
use App\Models\Users\UserHub;
use App\Models\Users\UserLigne;
use Illuminate\Http\Request;

class LigneController extends ApiController
{
    public function info(Request $request, $ligne_id)
    {
        $ligne = Ligne::find($ligne_id)->load('requirement');
        $hub = UserHub::find($request->get('user_hub_id'));

        //dd($request->all());

        //Si la ligne est déja acquise
        $verif_ligne = UserLigne::where('user_hub_id', $hub->id)->where('ligne_id', $ligne_id)->count();

        return $this->sendSuccess(null, [
            'error_ligne_acl' => $verif_ligne != 0 ? true : false,
            'prix_brut' => eur($ligne->price),
            'ligne' => $ligne,
        ]);
    }
}
