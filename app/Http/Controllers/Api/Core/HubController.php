<?php

namespace App\Http\Controllers\Api\Core;

use App\Http\Controllers\Api\ApiController;
use App\Models\Core\Hub;
use App\Models\User;
use Illuminate\Http\Request;

class HubController extends ApiController
{
    public function index()
    {
        return response()->json(Hub::all());
    }

    public function info(Request $request)
    {
        $hub = Hub::find($request->get('hub_id'));
        $user = User::find($request->get('user_id'));
        $balance = $user->balance;
        $amount_du = $hub->price - ($hub->price * $user->company->subvention / 100);

        return $this->sendSuccess(null, [
            'hub' => $hub,
            'logo_ter' => $hub->ter ? '/storages/logos/ter.png' : null,
            'logo_tgv' => $hub->tgv ? '/storages/logos/tgv.png' : null,
            'logo_intercite' => $hub->intercity ? '/storages/logos/intercite.png' : null,
            'prix_brut' => eur($hub->price),
            'subvention_percent' => $user->company->subvention.'%',
            'subvention' => eur($hub->price * $user->company->subvention / 100),
            'restant' => eur($hub->price - ($hub->price * $user->company->subvention / 100)), /** @phpstan-ignore-line */
            'dispo' => $amount_du > $balance ? false : true,
            'possess' => $user->hubs()->where('hub_id', $request->get('hub_id'))->get()->count() == 0 ? false : true,
        ]);
    }
}
