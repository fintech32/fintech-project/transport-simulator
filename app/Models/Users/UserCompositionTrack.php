<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Users\UserCompositionTrack
 *
 * @property int $id
 * @property int $user_composition_id
 * @property int $user_engine_id
 * @property-read \App\Models\Users\UserComposition|null $composition
 * @property-read \App\Models\Users\UserEngine|null $engine
 * @property-read \App\Models\Users\UserLigne|null $ligne
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompositionTrack newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompositionTrack newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompositionTrack query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompositionTrack whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompositionTrack whereUserCompositionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompositionTrack whereUserEngineId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperUserCompositionTrack
 */
class UserCompositionTrack extends Model
{
    public $timestamps = false;

    public function composition(): BelongsTo
    {
        return $this->belongsTo(UserComposition::class);
    }

    public function engine(): BelongsTo
    {
        return $this->belongsTo(UserEngine::class);
    }

    public function ligne(): HasOne
    {
        return $this->hasOne(UserLigne::class);
    }
}
