<?php

namespace App\Models\Users;

use App\Models\Core\Ligne;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Users\UserLigne
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon $date_achat
 * @property int $nb_depart_jour
 * @property int $user_hub_id
 * @property int $ligne_id
 * @property int|null $user_composition_track_id
 * @property-read \App\Models\Users\UserHub $hub
 * @property-read Ligne $ligne
 * @property-read \App\Models\Users\UserCompositionTrack|null $track
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserLigne newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLigne newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLigne query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserLigne whereDateAchat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLigne whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLigne whereLigneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLigne whereNbDepartJour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLigne whereUserCompositionTrackId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserLigne whereUserHubId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperUserLigne
 */
class UserLigne extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'date_achat' => 'datetime',
    ];

    public function hub()
    {
        return $this->belongsTo(UserHub::class);
    }

    public function ligne()
    {
        return $this->belongsTo(Ligne::class);
    }

    public function track(): BelongsTo
    {
        return $this->belongsTo(UserCompositionTrack::class);
    }
}
