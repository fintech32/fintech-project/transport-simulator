<?php

namespace App\Models\Users;

use App\Models\Compta\CompanySituation;
use App\Models\Compta\Mouvement;
use App\Models\User;
use App\Trait\CompanyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Users\UserCompany
 *
 * @property int $id
 * @property int $general_rank
 * @property int $distraction
 * @property int $tarification
 * @property int $ponctualite
 * @property int $securite
 * @property int $confort
 * @property int $rent_aux
 * @property int $frais
 * @property string $valorisation
 * @property string $beneficie_hebdo_travel
 * @property string $masse_salarial
 * @property string $treso_structure
 * @property string $last_impot
 * @property string $location_mensual_fee
 * @property string $remb_mensual
 * @property int $user_id
 * @property-read CompanySituation|null $situation
 * @property-read User $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereBeneficieHebdoTravel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereConfort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereDistraction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereFrais($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereGeneralRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereLastImpot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereLocationMensualFee($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereMasseSalarial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany wherePonctualite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereRembMensual($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereRentAux($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereSecurite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereTarification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereTresoStructure($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserCompany whereValorisation($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperUserCompany
 */
class UserCompany extends Model
{
    use CompanyTrait;

    public $timestamps = false;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function mouvements()
    {
        return $this->hasMany(Mouvement::class);
    }

    public function situation(): HasOne
    {
        return $this->hasOne(CompanySituation::class);
    }

    public static function calcSubvention($tresorerie_structurel): int
    {
        return match (true) {
            $tresorerie_structurel <= 12000 => 10,
            $tresorerie_structurel <= 29000 => 23,
            $tresorerie_structurel <= 42000 => 32,
            $tresorerie_structurel <= 60000 => 41,
            $tresorerie_structurel <= 79000 => 56,
            $tresorerie_structurel <= 90000 => 68,
            $tresorerie_structurel <= 150000 => 80,
            default => 85
        };
    }

    public static function calcImpot()
    {
        return 0;
    }
}
