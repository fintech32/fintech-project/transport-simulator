<?php

namespace App\Models\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Users\UserComposition
 *
 * @property int $id
 * @property int $user_id
 * @property int $user_hub_id
 * @property-read User $User
 * @property-read \App\Models\Users\UserHub $hub
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Users\UserCompositionTrack> $tracks
 * @property-read int|null $tracks_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserComposition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserComposition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserComposition query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserComposition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserComposition whereUserHubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserComposition whereUserId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperUserComposition
 */
class UserComposition extends Model
{
    public $timestamps = false;

    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function hub(): BelongsTo
    {
        return $this->belongsTo(UserHub::class);
    }

    public function tracks(): HasMany
    {
        return $this->hasMany(UserCompositionTrack::class);
    }
}
