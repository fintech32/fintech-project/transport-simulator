<?php

namespace App\Models\Users;

use App\Models\Core\Engine;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Users\UserEngine
 *
 * @property int $id
 * @property int $max_runtime_engine Nombre de fois utiliser avant maintenance
 * @property int $user_id
 * @property int $engine_id
 * @property int $user_hub_id
 * @property-read Engine $engine
 * @property-read \App\Models\Users\UserHub $hub
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Users\UserCompositionTrack> $tracks
 * @property-read int|null $tracks_count
 * @property-read User $user
 *
 * @method static \Illuminate\Database\Eloquent\Builder|UserEngine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEngine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEngine query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEngine whereEngineId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEngine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEngine whereMaxRuntimeEngine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEngine whereUserHubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEngine whereUserId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperUserEngine
 */
class UserEngine extends Model
{
    public $timestamps = false;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function engine(): BelongsTo
    {
        return $this->belongsTo(Engine::class);
    }

    public function hub()
    {
        return $this->belongsTo(UserHub::class);
    }

    public function tracks(): HasMany
    {
        return $this->hasMany(UserCompositionTrack::class);
    }
}
