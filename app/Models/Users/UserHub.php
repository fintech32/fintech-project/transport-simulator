<?php

namespace App\Models\Users;

use App\Models\Core\Hub;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @mixin IdeHelperUserHub
 */
class UserHub extends Model
{
    use HasFactory;

    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'date_achat' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function hub()
    {
        return $this->belongsTo(Hub::class);
    }

    public function lignes()
    {
        return $this->hasMany(UserLigne::class);
    }

    public function engines(): HasMany
    {
        return $this->hasMany(UserEngine::class);
    }

    public function compositions(): HasMany
    {
        return $this->hasMany(UserComposition::class);
    }
}
