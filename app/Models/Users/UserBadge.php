<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperUserBadge
 */
class UserBadge extends Model
{
    protected $guarded = [];
}
