<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\LigneStation
 *
 * @property int $id
 * @property string $name
 * @property string $time
 * @property int $ligne_id
 * @property-read \App\Models\Core\Ligne $ligne
 *
 * @method static \Illuminate\Database\Eloquent\Builder|LigneStation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LigneStation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LigneStation query()
 * @method static \Illuminate\Database\Eloquent\Builder|LigneStation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LigneStation whereLigneId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LigneStation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LigneStation whereTime($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperLigneStation
 */
class LigneStation extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function ligne()
    {
        return $this->belongsTo(Ligne::class);
    }
}
