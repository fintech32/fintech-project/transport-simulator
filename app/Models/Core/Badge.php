<?php

namespace App\Models\Core;

use App\Models\User;
use App\Models\Users\UserBadge;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperBadge
 */
class Badge extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'action',
        'action_count',
    ];

    public function users()
    {
        return $this->hasMany(UserBadge::class);
    }

    public function rewards()
    {
        return $this->hasMany(BadgeReward::class);
    }

    public function isUnlockedFor(User $user): bool
    {
        return $this->users()
            ->where('user_id', $user->id)
            ->exists();
    }

    public function unlockActionFor(User $user, string $action, int $count = 0)
    {
        $badge = $this->newQuery()
            ->where('action', $action)
            ->where('action_count', $count)
            ->first();

        if ($badge && ! $badge->isUnlockedFor($user)) {
            UserBadge::create([
                'user_id' => $user->id,
                'badge_id' => $badge->id,
            ]);

            return $badge;
        }

        return null;
    }
}
