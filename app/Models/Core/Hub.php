<?php

namespace App\Models\Core;

use App\Models\Users\UserHub;
use Bavix\Wallet\Interfaces\Customer;
use Bavix\Wallet\Interfaces\ProductInterface;
use Bavix\Wallet\Traits\HasWallet;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperHub
 */
class Hub extends Model implements ProductInterface
{
    use HasWallet;

    public $timestamps = false;

    protected $guarded = [];

    protected $appends = ['count_annual_passengers'];

    public function lignes()
    {
        return $this->hasMany(Ligne::class);
    }

    public function requirement()
    {
        return $this->hasMany(LigneRequireHub::class);
    }

    public function users()
    {
        return $this->hasMany(UserHub::class);
    }

    public function getAmountProduct(Customer $customer): int|string
    {
        // TODO: Implement getAmountProduct() method.
        /** @phpstan-ignore-next-line */
        return $this->price;
    }

    public function getMetaProduct(): ?array
    {
        // TODO: Implement getMetaProduct() method.
        return [
            'title' => $this->name,
            'description' => 'Achat du hub: '.$this->name,
        ];
    }

    public function getCountAnnualPassengersAttribute(): float|int
    {
        return (($this->request_first_class + $this->request_second_class) * 20) * 240; // 240 jours de travail annuel
    }
}
