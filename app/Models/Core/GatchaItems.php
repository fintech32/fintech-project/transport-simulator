<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class GatchaItems extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
        'action_reward',
        'rate',
        'dropup',
        'dropup_rate',
        'image',
    ];
}
