<?php

namespace App\Models\Core;

use App\Models\Users\UserLigne;
use Bavix\Wallet\Interfaces\Customer;
use Bavix\Wallet\Interfaces\ProductInterface;
use Bavix\Wallet\Traits\HasWallet;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\Ligne
 *
 * @property int $id
 * @property string $station_start
 * @property string $station_end
 * @property string $nb_station
 * @property string $price
 * @property string $distance
 * @property string $type_vehicule
 * @property string $time_min
 * @property int $hub_id
 * @property-read string $balance
 * @property-read int $balance_int
 * @property-read \Bavix\Wallet\Models\Wallet $wallet
 * @property-read \App\Models\Core\Hub $hub
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Core\LigneRequireHub> $requirement
 * @property-read int|null $requirement_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Core\LigneStation> $stations
 * @property-read int|null $stations_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Bavix\Wallet\Models\Transaction> $transactions
 * @property-read int|null $transactions_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Bavix\Wallet\Models\Transfer> $transfers
 * @property-read int|null $transfers_count
 * @property-read UserLigne|null $users
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Bavix\Wallet\Models\Transaction> $walletTransactions
 * @property-read int|null $wallet_transactions_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne whereDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne whereHubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne whereNbStation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne whereStationEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne whereStationStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne whereTimeMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ligne whereTypeVehicule($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperLigne
 */
class Ligne extends Model implements ProductInterface
{
    use HasWallet;

    public $timestamps = false;

    protected $guarded = [];

    public function hub()
    {
        return $this->belongsTo(Hub::class);
    }

    public function stations()
    {
        return $this->hasMany(LigneStation::class);
    }

    public function requirement()
    {
        return $this->hasMany(LigneRequireHub::class);
    }

    public function users()
    {
        return $this->belongsTo(UserLigne::class);
    }

    public function getAmountProduct(Customer $customer): int|string
    {
        // TODO: Implement getAmountProduct() method.
        return $this->price;
    }

    public function getMetaProduct(): ?array
    {
        // TODO: Implement getMetaProduct() method.
        return [
            'title' => $this->station_start.' - '.$this->station_end,
            'description' => 'Achat de la ligne: '.$this->station_start.' - '.$this->station_end,
        ];
    }
}
