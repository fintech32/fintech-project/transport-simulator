<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class VersionTask extends Model
{
    protected $guarded = [];

    public function version()
    {
        return $this->belongsTo(Version::class);
    }
}
