<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\LigneRequireHub
 *
 * @property int $id
 * @property int $ligne_id
 * @property int $hub_id
 * @property-read \App\Models\Core\Hub $hub
 * @property-read \App\Models\Core\Ligne $ligne
 *
 * @method static \Illuminate\Database\Eloquent\Builder|LigneRequireHub newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LigneRequireHub newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|LigneRequireHub query()
 * @method static \Illuminate\Database\Eloquent\Builder|LigneRequireHub whereHubId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LigneRequireHub whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|LigneRequireHub whereLigneId($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperLigneRequireHub
 */
class LigneRequireHub extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    public function hub()
    {
        return $this->belongsTo(Hub::class, 'hub_id');
    }

    public function ligne()
    {
        return $this->belongsTo(Ligne::class, 'ligne_id');
    }
}
