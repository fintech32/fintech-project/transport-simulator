<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Core\Annonce
 *
 * @property int $id
 * @property string $title
 * @property string $synopsis
 * @property string $description
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce query()
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce whereSynopsis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Annonce whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperAnnonce
 */
class Annonce extends Model
{
    protected $guarded = [];
}
