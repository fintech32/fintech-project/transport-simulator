<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperBadgeReward
 */
class BadgeReward extends Model
{
    public $timestamps = false;

    protected $appends = ['type_reward'];

    protected $fillable = [
        'type',
        'amount',
    ];

    public function badge()
    {
        return $this->belongsTo(Badge::class);
    }

    public function getTypeRewardAttribute()
    {
        return match ($this->type) {
            'argent' => eur($this->amount),
            't-point' => $this->amount.' T Point',
            'research' => $this->amount.' Point de Recherche'
        };
    }
}
