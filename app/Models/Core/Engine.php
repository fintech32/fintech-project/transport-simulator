<?php

namespace App\Models\Core;

use App\Models\Users\UserEngine;
use Bavix\Wallet\Interfaces\Customer;
use Bavix\Wallet\Interfaces\ProductInterface;
use Bavix\Wallet\Traits\HasWallet;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\Core\Engine
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $type_train
 * @property string $type_engine
 * @property string $velocity
 * @property int|null $nb_passager
 * @property string $price_achat
 * @property string $price_maintenance
 * @property int $duration_maintenance en jours
 * @property string $price_location
 * @property-read string $balance
 * @property-read int $balance_int
 * @property-read \Bavix\Wallet\Models\Wallet $wallet
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Bavix\Wallet\Models\Transaction> $transactions
 * @property-read int|null $transactions_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Bavix\Wallet\Models\Transfer> $transfers
 * @property-read int|null $transfers_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, UserEngine> $users
 * @property-read int|null $users_count
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \Bavix\Wallet\Models\Transaction> $walletTransactions
 * @property-read int|null $wallet_transactions_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Engine newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Engine newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Engine query()
 * @method static \Illuminate\Database\Eloquent\Builder|Engine whereDurationMaintenance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine whereNbPassager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine wherePriceAchat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine wherePriceLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine wherePriceMaintenance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine whereTypeEngine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine whereTypeTrain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Engine whereVelocity($value)
 *
 * @mixin \Eloquent
 * @mixin IdeHelperEngine
 */
class Engine extends Model implements ProductInterface
{
    use HasWallet;

    public $timestamps = false;

    protected $guarded = [];

    public function users(): HasMany
    {
        return $this->hasMany(UserEngine::class);
    }

    public function getAmountProduct(Customer $customer): int|string
    {
        // TODO: Implement getAmountProduct() method.
        return $this->price_achat;
    }

    public function getMetaProduct(): ?array
    {
        return [
            'title' => $this->name,
            'description' => "Achat de l'engin: ".$this->name,
        ];
    }
}
