<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Models\Users\UserCompany;
use App\Models\Users\UserComposition;
use App\Models\Users\UserEngine;
use App\Models\Users\UserHub;
use App\Trait\Badgeable;
use Bavix\Wallet\Interfaces\Wallet;
use Bavix\Wallet\Traits\CanPay;
use Bavix\Wallet\Traits\HasWallet;
use Bavix\Wallet\Traits\HasWallets;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @mixin IdeHelperUser
 */
class User extends Authenticatable implements Wallet
{
    use HasApiTokens, HasFactory, Notifiable, HasWallet, HasWallets, CanPay, Badgeable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'avatar_secretary',
        'name_secretary',
        'name_company',
        'logo_company',
        'desc_company',
        'premium',
        'strip_customer',
        'argent',
        'tpoint',
        'research',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    protected $appends = ['status_account'];

    public function company()
    {
        return $this->hasOne(UserCompany::class);
    }

    public function hubs()
    {
        return $this->hasMany(UserHub::class);
    }

    public function engines(): HasMany
    {
        return $this->hasMany(UserEngine::class);
    }

    public function compositions(): HasMany
    {
        return $this->hasMany(UserComposition::class);
    }

    public function getStatusAccountAttribute()
    {
        if (! empty($this->email_verified_at)) {
            return '<span class="badge badge-light-success fw-bold fs-8 px-2 py-1 ms-2">Vérifier</span>';
        } else {
            return '<a href=""><span class="badge badge-light-danger fw-bold fs-8 px-2 py-1 ms-2">Non Vérifier</span></a>';
        }
    }
}
