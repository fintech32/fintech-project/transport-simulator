<?php

namespace App\Models\Compta;

use App\Models\Users\UserCompany;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin IdeHelperMouvement
 */
class Mouvement extends Model
{
    protected $fillable = [
        'title',
        'amount',
        'type_account',
        'type_mvm',
        'user_company_id',
    ];

    public function company()
    {
        return $this->belongsTo(UserCompany::class);
    }
}
