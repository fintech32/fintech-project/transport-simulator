<?php

namespace App\Models\Compta;

use App\Models\Users\UserCompany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @mixin IdeHelperCompanySituation
 */
class CompanySituation extends Model
{
    public $timestamps = false;

    protected $guarded = [];

    protected $casts = [
        'date' => 'date',
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(UserCompany::class);
    }
}
