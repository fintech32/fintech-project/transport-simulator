<?php

namespace App\Helpers\Badges;

use App\Models\Core\Badge;
use App\Models\User;
use App\Notifications\BadgeUnlockNotification;
use App\Trait\ComptaTrait;

class BadgeSubscriber
{
    public function __construct(private readonly Badge $badge)
    {
    }

    public function subscribe($events)
    {
        $events->listen(\App\Events\PremiumEvent::class, $this->onPremium(...));
        $events->listen('eloquent.saved: App\Models\Users\UserHub', $this->onCheckoutFirstHub(...));
        $events->listen('eloquent.saved: App\Models\Users\UserHub', $this->onCheckoutSecondHub(...));
    }

    public function notifyBadgeUnlock(User $user, Badge $badge)
    {
        $user->notify(new BadgeUnlockNotification($badge));
    }

    public function onPremium($event)
    {
        $badge = $this->badge->unlockActionFor($event->user, 'premium');
        ComptaTrait::createCredit($event->user, 'Avantage Premium', 10000, 'produit', 'subvention', false);
        //$this->notifyBadgeUnlock($event->user, $this->badge);
    }

    public function onCheckoutFirstHub($userHub)
    {
        $user = $userHub->user;
        $hub_count = $user->hubs()->count();
        ComptaTrait::createCredit($user, 'Récompense', 100000, 'produit', 'subvention', false);
        $badge = $this->badge->unlockActionFor($user, 'checkoutFirstHub', $hub_count);
        //$this->notifyBadgeUnlock($user, $this->badge);
    }

    public function onCheckoutSecondHub($userHub)
    {
        $user = $userHub->user;
        $hub_count = $user->hubs()->count();
        ComptaTrait::createCredit($user, 'Récompense', 150000, 'produit', 'subvention', false);
        $badge = $this->badge->unlockActionFor($user, 'checkoutSecondHub', $hub_count);
        //$this->notifyBadgeUnlock($user, $this->badge);
    }
}
