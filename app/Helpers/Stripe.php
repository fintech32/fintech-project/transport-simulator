<?php

namespace App\Helpers;

use Stripe\StripeClient;

class Stripe
{
    public StripeClient $client;

    public function __construct()
    {
        $this->client = new StripeClient(config('services.stripe.secret_key'));
    }
}
