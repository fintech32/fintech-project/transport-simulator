<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class InputDate extends Component
{
    /**
     * @var null
     */
    public $placeholder;

    /**
     * @var null
     */
    public $helpText;

    /**
     * @var null
     */
    public $text;

    /**
     * @var null
     */
    public $class;

    /**
     * Create a new component instance.
     *
     * @param  string  $label
     * @param  string  $value
     * @param  bool  $required
     * @param  bool  $autofocus
     * @param  null  $placeholder
     * @param  bool  $help
     * @param  null  $helpText
     * @param  null  $text
     * @param  null  $class
     */
    public function __construct(
        public $name, public $type = 'text', public $label = '',
        public $value = '', public $required = false, public $autofocus = false,
        $placeholder = null, public $help = false, $helpText = null,
        $text = null, $class = null
    ) {
        $this->placeholder = $placeholder;
        $this->helpText = $helpText;
        $this->text = $text;
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.input-date');
    }
}
