<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Select extends Component
{
    /**
     * @var null
     */
    public $placeholder;

    /**
     * @var bool
     */
    public $required;

    public function __construct(
        public $name, public $datas, public $label, $placeholder = null, bool $required = false, public string|int|null $value = null
    ) {
        $this->placeholder = $placeholder;
        $this->required = $required;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.select');
    }
}
