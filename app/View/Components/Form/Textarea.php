<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Textarea extends Component
{
    /**
     * @var null
     */
    public $value;

    /**
     * Create a new component instance.
     *
     * @param  bool  $required
     * @param  null  $value
     */
    public function __construct(public $name, public $label, public $required = false, $value = null)
    {
        $this->value = $value;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.textarea');
    }
}
