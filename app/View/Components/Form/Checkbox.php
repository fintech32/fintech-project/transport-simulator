<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Checkbox extends Component
{
    /**
     * @var null
     */
    public $class;

    /**
     * Create a new component instance.
     *
     * @param  bool  $checked
     */
    public function __construct(public $name, public $label, public $value, public $checked = false, $class = null)
    {
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.checkbox');
    }
}
