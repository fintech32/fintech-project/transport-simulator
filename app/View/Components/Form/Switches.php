<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Switches extends Component
{
    /**
     * Create a new component instance.
     *
     * @param  null  $check
     */
    public function __construct(public $name, public $label, public $value, public $check = null)
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.switch');
    }
}
