<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Radio extends Component
{
    /**
     * @var null
     */
    public $function;

    /**
     * @var null
     */
    public $nameFunction;

    /**
     * Create a new component instance.
     *
     * @param  bool  $checked
     * @param  null  $function
     * @param  null  $nameFunction
     */
    public function __construct(public $name, public $value, public $for, public $label, public $checked = false, $function = null, $nameFunction = null)
    {
        $this->function = $function;
        $this->nameFunction = $nameFunction;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.radio');
    }
}
