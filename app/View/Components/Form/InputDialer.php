<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class InputDialer extends Component
{
    /**
     * @var null
     */
    public $prefix;

    /**
     * Create a new component instance.
     *
     * @param  null  $prefix
     * @param  bool  $required
     */
    public function __construct(public $name, public $label, public $min, public $max, public $step, public $value, $prefix = null, public $required = false)
    {
        $this->prefix = $prefix;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.input-dialer');
    }
}
