<?php

namespace App\View\Components\Form;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class InputDateTime extends Component
{
    /**
     * @var null
     */
    public $placeholder;

    /**
     * @var null
     */
    public $class;

    public function __construct(public $name, public $label, public bool $required = false, $placeholder = null, $class = null)
    {
        $this->placeholder = $placeholder;
        $this->class = $class;
    }

    public function render(): View
    {
        return view('components.input-date-time');
    }
}
