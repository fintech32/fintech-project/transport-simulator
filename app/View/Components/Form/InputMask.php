<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class InputMask extends Component
{
    /**
     * Create a new component instance.
     *
     * @param  bool  $required
     */
    public function __construct(public $name, public $label, public $mask, public $required = true)
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.input-mask');
    }
}
