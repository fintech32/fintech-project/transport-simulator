<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class InputGroup extends Component
{
    /**
     * @var null
     */
    public $value;

    /**
     * @var null
     */
    public $placeholder;

    /**
     * @var null
     */
    public $text;

    /**
     * @var null
     */
    public $class;

    /**
     * Create a new component instance.
     *
     * @param  null  $value
     * @param  bool  $required
     * @param  null  $placeholder
     * @param  null  $text
     * @param  null  $class
     */
    public function __construct(
        public $name, public $symbol, public $placement, public $label = null, $value = null,
        public $required = false, $placeholder = null, $text = null, $class = null
    ) {
        $this->value = $value;
        $this->placeholder = $placeholder;
        $this->text = $text;
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.input-group');
    }
}
