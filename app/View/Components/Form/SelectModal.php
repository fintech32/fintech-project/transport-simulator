<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class SelectModal extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(public $name, public mixed $datas, public $label, public mixed $placeholder = null, public bool $required = false, public string|int|null $value = null)
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.select-modal');
    }
}
