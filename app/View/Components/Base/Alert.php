<?php

namespace App\View\Components\Base;

use Illuminate\View\Component;

class Alert extends Component
{
    /**
     * @var null
     */
    public $buttons;

    /**
     * Create a new component instance.
     *
     * @param  null  $buttons
     * @param  null  $class
     * @param  null  $id
     */
    public function __construct(public $type, public $color, public $icon, public $title, public $content, $buttons = null, public mixed $class = null, public mixed $id = null)
    {
        $this->buttons = $buttons;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.base.alert');
    }
}
