<?php

namespace App\View\Components\Base;

use Illuminate\View\Component;

class Underline extends Component
{
    /**
     * Create a new component instance.
     *
     * @param  int  $size
     * @param  string  $color
     * @param  string  $sizeText
     */
    public function __construct(public $title, public $size = 8, public $color = 'primary', public $sizeText = 'fs-2tx', public string $class = 'w-250px mt-5 mb-5')
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.base.underline');
    }
}
