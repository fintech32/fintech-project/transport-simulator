<?php

namespace App\View\Components\Base;

use Illuminate\View\Component;

class Icon extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(public $icon, public string $size = 'fs-2', public string $margin = 'me-2', public string $color = 'primary')
    {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.base.icon');
    }
}
