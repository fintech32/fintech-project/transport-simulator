<?php

namespace App\View\Components\Base;

use Illuminate\View\Component;

class Indicator extends Component
{
    /**
     * @var null
     */
    public $text;

    /**
     * Create a new component instance.
     *
     * @param  null  $text
     */
    public function __construct(public bool $textOnly = true, $text = null)
    {
        $this->text = $text;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.base.indicator');
    }
}
