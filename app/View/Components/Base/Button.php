<?php

namespace App\View\Components\Base;

use Illuminate\View\Component;

class Button extends Component
{
    /**
     * @var null
     */
    public $other;

    /**
     * @var null
     */
    public $tooltip;

    /**
     * Create a new component instance.
     *
     * @param  array  $datas
     * @param  null  $other
     * @param  string  $textIndicator
     * @param  null  $tooltip
     */
    public function __construct(
        public $class, public $text, public $datas = [], public $id = null, $other = null,
        public $textIndicator = 'Veuillez Patienter...', $tooltip = null
    ) {
        $this->other = $other;
        $this->tooltip = $tooltip;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.base.button');
    }
}
