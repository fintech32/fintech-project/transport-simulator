<?php

namespace App\Actions\Fortify;

use App\Helpers\Stripe;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Stripe\Exception\ApiErrorException;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array<string, string>  $input
     *
     * @throws ApiErrorException
     */
    public function create(array $input): User
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
        ])->validate();

        $stripe = new Stripe();
        $customer = $stripe->client->customers->create([
            'email' => $input['email'],
            'name' => $input['name'],
        ]);

        return User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'argent' => 5_000_000,
            'tpoint' => 200,
            'research' => 50000,
            'stripe_customer' => $customer->id,
        ]);
    }
}
