<?php

namespace App\Console;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Log;

class UpdateCommand extends Command
{
    protected $signature = 'update';

    protected $description = 'Update the application to the latest version';

    public function handle(): void
    {
        $client = new Client();
        try {
            $response = $client->request('GET', 'https://gitlab.com/api/v4/projects/46389173/repository/branches?private_token='.config('app.gitlab_token'));
        } catch (GuzzleException $e) {
            dd($e);
        }
        $branches = json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR);

        $latest_branch = null;
        $latest_commit = null;
        foreach ($branches as $branch) {
            if ($branch['name'] === 'production') {
                $latest_branch = $branch['name'];
                $latest_commit = $branch['commit']['id'];
                break;
            }
        }

        if ($latest_branch && $latest_commit) {
            exec('git fetch');
            exec("git checkout $latest_branch");
            exec('git reset --hard origin/'.$latest_branch);
            exec('composer install');
            exec('php artisan migrate --force');
            exec('php artisan db:seed VersionSeeder');
            Log::info('Application updated to the latest version: '.$latest_commit);
        } else {
            Log::info('No updates found');
        }
    }
}
