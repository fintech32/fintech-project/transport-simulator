<?php

namespace App\Notifications;

use App\Models\Core\Badge;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BadgeUnlockNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public function __construct(private readonly Badge $badge)
    {
    }

    public function via($notifiable): array
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(mixed $notifiable): MailMessage
    {
        return (new MailMessage)
            ->line('Vous avez débloqué la badge '.$this->badge->name)
            ->line('Bravo !');
    }

    public function toDatabase($notifiable): array
    {
        return [
            'type' => 'success',
            'icon' => 'ki-abstract-4',
            'title' => 'Succès déverrouiller',
            'description' => "Vous avez débloqué le badge: <strong>{$this->badge->name}</strong>",
            'time' => now(),
        ];
    }

    public function toArray($notifiable): array
    {
        return [
            'type' => 'success',
            'icon' => 'ki-abstract-4',
            'title' => 'Succès déverrouiller',
            'description' => "Vous avez débloqué le badge: <strong>{$this->badge->name}</strong>",
            'time' => now(),
        ];
    }
}
