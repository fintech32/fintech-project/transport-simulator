---
name: "Nouvelle Fonctionnalité lié à un problème"
about: Suggérer une nouvelle fonctionnalité au projet
---

**Votre demande de fonctionnalité est-elle liée à un problème ? Décrivez s'il vous plait.**


**Décrivez la solution que vous souhaitez**


**Décrivez les alternatives que vous avez envisagées**
