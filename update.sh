#!/bin/bash
git pull
git tag -l | tail -1 >> version
git checkout $version

composer install --ignore-platform-reqs --no-interaction
npm install
chmod -R 777 storage/ bootstrap/
npm run build
php artisan migrate --force
php artisan db:seed UpdateSeeder
php artisan db:seed VersionSeeder
php artisan cache:clear


