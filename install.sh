#!/bin/bash
ENVIRONMENT="Environnement"
envirs=("test" "prod")

composer install --ignore-platform-reqs --no-interaction
npm install
chmod -R 777 storage/ bootstrap/
select item in "${envirs[@]}"; do
    case $item in
    "test")
        cp .env.testing .env
        npm run build
        php artisan key:generate
        php artisan storage:link
        php artisan migrate
        php artisan db:seed InstallSeeder
        php artisan db:seed TestSeeder
        php artisan db:seed VersionSeeder
        php artisan cache:clear
        exit
        ;;
    "prod")
        cp .env.prod .env
        npm run build
        php artisan key:generate
        php artisan storage:link
        php artisan migrate
        php artisan db:seed InstallSeeder
        php artisan db:seed VersionSeeder
        php artisan cache:clear
        exit
        ;;
    *)
        echo "Erreur lors du choix du système"
        exit
        ;;
    esac
done



