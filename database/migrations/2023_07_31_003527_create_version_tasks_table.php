<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('version_tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->enum('priority', ['mineur', 'normal', 'majeur', 'critique', 'bloquant'])->default('mineur');
            $table->enum('type', ['issues', 'incident', 'bug', 'visuel', 'fonction', 'probleme_performance'])->default('issues');
            $table->enum('status', ['ouvert', 'revue', 'verifie', 'cloturer'])->default('ouvert');
            $table->timestamps();

            $table->foreignId('version_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('version_tasks');
    }
};
