<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('ligne_stations', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('time')->default(0); // Temps entre deux station
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();

            $table->foreignId('ligne_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('ligne_stations');
    }
};
