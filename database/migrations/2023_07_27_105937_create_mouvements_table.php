<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('mouvements', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->decimal('amount', 64, 0);
            $table->enum('type_account', ['charge', 'produit']);
            $table->enum('type_mvm', ['electricite', 'salaire_technicien', 'salaire_commerce', 'salaire_voyageur', 'salaire_reseau', 'pret', 'interet', 'taxe', 'maintenance_vehicule', 'maintenance_technicentre', 'billetterie', 'commerce', 'publicite', 'parking', 'impot', 'subvention', 'achat_materiel', 'achat_hub', 'achat_ligne', 'location_materiel']);
            $table->timestamps();

            $table->foreignId('user_company_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('user_hub_id')
                ->nullable()
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('mouvements');
    }
};
