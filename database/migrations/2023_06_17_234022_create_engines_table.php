<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('engines', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->enum('type_train', ['ter', 'tgv', 'ic', 'other', 'consist']);
            $table->enum('type_engine', ['locomotive', 'voiture', 'automoteur', 'automotrice']);
            $table->string('velocity');
            $table->integer('nb_passager')->nullable();
            $table->string('price_achat');
            $table->string('price_maintenance');
            $table->integer('duration_maintenance')->comment('en jours')->default(1);
            $table->string('price_location');

        });
    }

    public function down(): void
    {
        Schema::dropIfExists('engines');
    }
};
