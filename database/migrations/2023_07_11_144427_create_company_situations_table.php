<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('company_situations', function (Blueprint $table) {
            $table->id();
            $table->date('date')->default(now()->subMonth()->endOfMonth());
            $table->decimal('turnover', 16)->default(0);
            $table->decimal('cost_travel', 16)->default(0);
            $table->decimal('remb_emprunt', 16)->default(0);
            $table->decimal('coast_location', 16)->default(0);
            $table->decimal('structural_cash', 16)->default(0);
            $table->decimal('benefice', 16)->default(0);

            $table->foreignId('user_company_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('company_situations');
    }
};
