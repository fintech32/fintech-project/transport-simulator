<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('hubs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('nb_quai');
            $table->boolean('ter')->default(false);
            $table->boolean('tgv')->default(false);
            $table->boolean('intercity')->default(false);
            $table->decimal('price', 16);
            $table->decimal('taxe_hub', 16);
            $table->integer('request_first_class');
            $table->integer('request_second_class');
            $table->string('region');
            $table->string('pays');
            $table->integer('long_quai');
            $table->boolean('commerce')->default(true);
            $table->boolean('publicite')->default(true);
            $table->boolean('parking')->default(true);
            $table->integer('nb_slot_commerce')->default(0);
            $table->integer('nb_slot_publicite')->default(0);
            $table->integer('nb_slot_parking')->default(0);
            $table->integer('nb_luminaire')->default(0);
            $table->integer('nb_ecran_quai')->default(0);
            $table->integer('nb_ecran_pub')->default(0);
            $table->integer('nb_pc')->default(0);
            $table->integer('nb_chauffage')->default(0);
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->integer('superficie_infra')->nullable();
            $table->integer('superficie_quai')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('hubs');
    }
};
