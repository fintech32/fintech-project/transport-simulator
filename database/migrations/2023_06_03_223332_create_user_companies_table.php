<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('user_companies', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('general_rank');
            $table->integer('distraction');
            $table->integer('tarification');
            $table->integer('ponctualite');
            $table->integer('securite');
            $table->integer('confort');
            $table->integer('rent_aux');
            $table->integer('frais');

            $table->decimal('valorisation', 16)->default(0);
            $table->decimal('beneficie_hebdo_travel', 16)->default(0);
            $table->decimal('masse_salarial', 16)->default(0);
            $table->decimal('treso_structure', 16)->default(0);
            $table->decimal('last_impot', 16)->default(0);
            $table->decimal('location_mensual_fee', 16)->default(0);
            $table->decimal('remb_mensual', 16)->default(0);

            $table->integer('subvention')->default(0);

            $table->foreignId('user_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('user_companies');
    }
};
