<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('lignes', function (Blueprint $table) {
            $table->id();
            $table->string('station_start');
            $table->string('station_end');
            $table->string('nb_station');
            $table->decimal('price', 16);
            $table->string('distance');
            $table->enum('type_vehicule', ['ter', 'tgv', 'ic']);
            $table->string('time_min')->default(0);

            $table->foreignId('hub_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('lignes');
    }
};
