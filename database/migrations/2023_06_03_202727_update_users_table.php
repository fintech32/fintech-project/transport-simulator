<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('avatar')->default('default.png');
            $table->string('name_secretary')->nullable();
            $table->string('avatar_secretary')->default('default.png');
            $table->string('name_company')->nullable();
            $table->string('logo_company')->default('logo.png');
            $table->text('desc_company')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->removeColumn('avatar');
            $table->removeColumn('name_secretary');
            $table->removeColumn('avatar_secretary');
            $table->removeColumn('name_company');
            $table->removeColumn('logo_company');
            $table->removeColumn('desc_company');
        });
    }
};
