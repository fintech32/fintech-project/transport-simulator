<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('premium')->default(false);
            $table->decimal('argent', 64, 0)->default(0);
            $table->decimal('tpoint', 64, 0)->default(0);
            $table->decimal('research', 64, 0)->default(0);
        });
    }

    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->removeColumn('premium');
            $table->removeColumn('argent');
            $table->removeColumn('tpoint');
            $table->removeColumn('reseach');
        });
    }
};
