<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('gatcha_items', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('action_reward');
            $table->string('rate');
            $table->boolean('dropup');
            $table->string('dropup_rate');
            $table->string('image')->nullable();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('gatcha_items');
    }
};
