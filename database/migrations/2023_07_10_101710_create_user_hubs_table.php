<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_hubs', function (Blueprint $table) {
            $table->id();
            $table->integer('nb_salarie_iv')->default(0)->comment('Infra Voyage');
            $table->integer('nb_salarie_com')->default(0)->comment('Commerce');
            $table->integer('nb_salarie_ir')->default(0)->comment('Infra Reseau');
            $table->integer('nb_salarie_technicentre')->default(0);
            $table->timestamp('date_achat')->nullable();
            $table->integer('km_ligne')->default(0);

            $table->foreignId('user_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignId('hub_id')
                ->constrained()
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_hubs');
    }
};
