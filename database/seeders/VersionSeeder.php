<?php

namespace Database\Seeders;

use App\Models\Core\Version;
use Illuminate\Database\Seeder;

class VersionSeeder extends Seeder
{
    public function run(): void
    {
        $versions = Version::all();

        foreach ($versions as $version) {
            $version->delete();
        }

        Version::create([
            'version' => '0.0.1',
            'stage' => 'production',
            'note' => '',
        ]);

    }
}
