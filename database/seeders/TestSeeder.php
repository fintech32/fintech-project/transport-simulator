<?php

namespace Database\Seeders;

use App\Models\Core\Annonce;
use App\Models\Core\Hub;
use App\Models\User;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    public function run(): void
    {
        $user = User::create([
            'name' => 'Syltheron',
            'email' => 'test@test.com',
            'password' => \Hash::make('password'),
            'email_verified_at' => now(),
            'name_secretary' => 'Julie Moreau',
            'name_company' => 'SNCF',
            'argent' => 5000000,
            'tpoint' => 500,
            'research' => 10000,
        ]);

        $company = $user->company()->create([
            'general_rank' => 0,
            'distraction' => 0,
            'tarification' => 0,
            'ponctualite' => 0,
            'securite' => 0,
            'confort' => 0,
            'rent_aux' => 0,
            'frais' => 0,
            'subvention' => 10,
        ]);

        $company->situation()->create();

        $user->hubs()->create([
            'date_achat' => now(),
            'user_id' => $user->id,
            'hub_id' => Hub::get()->random()->id,
        ]);

        Annonce::create([
            'title' => "Test d'une annonce",
            'synopsis' => "Ceci est la création d'une annonce système",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'status' => true,
        ]);
    }
}
