<?php

namespace Database\Seeders\Install;

use App\Models\Core\Badge;
use App\Models\Core\BadgeReward;
use Illuminate\Database\Seeder;

class BadgeSeeder extends Seeder
{
    public function run(): void
    {
        Badge::create([
            'name' => 'Compte Premium',
            'action' => 'premium',
            'action_count' => 0,
        ]);

        BadgeReward::create([
            'type' => 'argent',
            'amount' => 10000,
            'badge_id' => 1,
        ]);

        Badge::create([
            'name' => 'Achetez votre premier HUB',
            'action' => 'checkoutFirstHub',
            'action_count' => 1,
        ]);

        BadgeReward::create([
            'type' => 'argent',
            'amount' => 100000,
            'badge_id' => 2,
        ]);

        BadgeReward::create([
            'type' => 't-point',
            'amount' => 50,
            'badge_id' => 2,
        ]);

        Badge::create([
            'name' => 'Achetez votre second HUB',
            'action' => 'checkoutSecondHub',
            'action_count' => 2,
        ]);

        BadgeReward::create([
            'type' => 'argent',
            'amount' => 150000,
            'badge_id' => 3,
        ]);

        Badge::create([
            'name' => 'Achetez votre premiere Ligne',
            'action' => 'checkoutFirstLine',
            'action_count' => 1,
        ]);

        BadgeReward::create([
            'type' => 'argent',
            'amount' => 35000,
            'badge_id' => 4,
        ]);

        BadgeReward::create([
            'type' => 't-point',
            'amount' => 20,
            'badge_id' => 4,
        ]);

        Badge::create([
            'name' => 'Achetez votre seconde Ligne',
            'action' => 'checkoutSecondLine',
            'action_count' => 2,
        ]);

        BadgeReward::create([
            'type' => 'argent',
            'amount' => 50000,
            'badge_id' => 5,
        ]);
    }
}
