<?php

namespace Database\Seeders\Install;

use App\Models\Core\Hub;
use Illuminate\Database\Seeder;

class ParisHubSeeder extends Seeder
{
    public function run(): void
    {
        Hub::create([
            'name' => "Paris - Gare de l'Est",
            'nb_quai' => 29,
            'ter' => 1,
            'tgv' => 1,
            'intercity' => 0,
            'price' => 719756,
            'taxe_hub' => 124,
            'request_first_class' => 4,
            'request_second_class' => 15,
            'region' => 'Ile-de-France',
            'pays' => 'France',
            'long_quai' => 420,
            'commerce' => 1,
            'publicite' => 1,
            'parking' => 1,
            'nb_slot_commerce' => 266,
            'nb_slot_publicite' => 532,
            'nb_slot_parking' => 887,
            'nb_luminaire' => 1330,
            'nb_ecran_quai' => 1015,
            'nb_ecran_pub' => 50,
            'nb_pc' => 156,
            'nb_chauffage' => 554,
            'latitude' => 48.876807,
            'longitude' => 2.359367,
            'superficie_infra' => 1120,
            'superficie_quai' => 12180,
        ]);

        Hub::create([
            'name' => 'Paris - Gare Montparnasse',
            'nb_quai' => 28,
            'ter' => 1,
            'tgv' => 1,
            'intercity' => 0,
            'price' => 1172670,
            'taxe_hub' => 209,
            'request_first_class' => 6,
            'request_second_class' => 23,
            'region' => 'Ile-de-France',
            'pays' => 'France',
            'long_quai' => 320,
            'commerce' => 1,
            'publicite' => 1,
            'parking' => 1,
            'nb_slot_commerce' => 203,
            'nb_slot_publicite' => 407,
            'nb_slot_parking' => 678,
            'nb_luminaire' => 1017,
            'nb_ecran_quai' => 747,
            'nb_ecran_pub' => 50,
            'nb_pc' => 120,
            'nb_chauffage' => 424,
            'latitude' => 48.840416,
            'longitude' => 2.319452,
            'superficie_infra' => 1213,
            'superficie_quai' => 8960,
        ]);
    }
}
