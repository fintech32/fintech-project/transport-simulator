<?php

namespace Database\Seeders\Install\Paris;

use App\Models\Core\Ligne;
use App\Models\Core\LigneRequireHub;
use App\Models\Core\LigneStation;
use Illuminate\Database\Seeder;

class EstTerLigneSeeder extends Seeder
{
    public function run(): void
    {
        Ligne::create([
            'station_start' => "Paris - Gare de l'Est",
            'station_end' => 'Culmont - Chalindrey',
            'nb_station' => 8,
            'price' => 435744,
            'distance' => 306,
            'type_vehicule' => 'ter',
            'time_min' => 178,
            'hub_id' => 1,
        ]);

        LigneRequireHub::create([
            'ligne_id' => 1,
            'hub_id' => 1,
        ]);

        LigneStation::create(['name' => "Paris - Gare de l'Est", 'time' => 0, 'latitude' => 48.876807, 'longitude' => 2.359367, 'ligne_id' => 1]);
        LigneStation::create(['name' => 'Longueville', 'time' => 48, 'latitude' => 48.513928, 'longitude' => 3.250037, 'ligne_id' => 1]);
        LigneStation::create(['name' => 'Nogent-sur-Seine', 'time' => 14, 'latitude' => 48.498176, 'longitude' => 3.493949, 'ligne_id' => 1]);
        LigneStation::create(['name' => 'Romilly-sur-Seine', 'time' => 12, 'latitude' => 48.514510, 'longitude' => 3.735429, 'ligne_id' => 1]);
        LigneStation::create(['name' => 'Troyes', 'time' => 20, 'latitude' => 48.29736915707945, 'longitude' => 4.064293396593517, 'ligne_id' => 1]);
        LigneStation::create(['name' => 'Vendeuvre', 'time' => 17, 'latitude' => 48.24051331711051, 'longitude' => 4.467234912090356, 'ligne_id' => 1]);
        LigneStation::create(['name' => 'Bar-sur-Aube', 'time' => 13, 'latitude' => 48.238672531545284, 'longitude' => 4.706745154417206, 'ligne_id' => 1]);
        LigneStation::create(['name' => 'Chaumont', 'time' => 22, 'latitude' => 48.10984743645446, 'longitude' => 5.1346399679000285, 'ligne_id' => 1]);
        LigneStation::create(['name' => 'Langres', 'time' => 24, 'latitude' => 47.877013493225526, 'longitude' => 5.344467994867152, 'ligne_id' => 1]);
        LigneStation::create(['name' => 'Culmont - Chalindrey', 'time' => 8, 'latitude' => 47.810292821462404, 'longitude' => 5.443327027410873, 'ligne_id' => 1]);

        Ligne::create([
            'station_start' => "Paris - Gare de l'Est",
            'station_end' => 'Saint-Dizier',
            'nb_station' => 5,
            'price' => 174240,
            'distance' => 242,
            'type_vehicule' => 'ter',
            'time_min' => 144,
            'hub_id' => 1,
        ]);

        LigneRequireHub::create([
            'ligne_id' => 2,
            'hub_id' => 1,
        ]);

        LigneStation::create(['name' => "Paris - Gare de l'Est", 'time' => 0, 'latitude' => 48.876807, 'longitude' => 2.359367, 'ligne_id' => 2]);
        LigneStation::create(['name' => 'Château-Thierry', 'time' => 55, 'latitude' => 49.038294505165034, 'longitude' => 3.4093064814460528, 'ligne_id' => 2]);
        LigneStation::create(['name' => 'Dormans', 'time' => 13, 'latitude' => 49.07922655006212, 'longitude' => 3.6419034698136166, 'ligne_id' => 2]);
        LigneStation::create(['name' => 'Épernay', 'time' => 15, 'latitude' => 49.046361286617326, 'longitude' => 3.9598747967925934, 'ligne_id' => 2]);
        LigneStation::create(['name' => 'Châlons-en-Champagne', 'time' => 24, 'latitude' => 48.95538782752096, 'longitude' => 4.348688183296605, 'ligne_id' => 2]);
        LigneStation::create(['name' => 'Vitry-le-François', 'time' => 19, 'latitude' => 48.71806545579232, 'longitude' => 4.58766922560931, 'ligne_id' => 2]);
        LigneStation::create(['name' => 'Saint-Dizier', 'time' => 18, 'latitude' => 48.64259099738957, 'longitude' => 4.9480240256048145, 'ligne_id' => 2]);
    }
}
