<?php

namespace Database\Seeders;

use Database\Seeders\Install\BadgeSeeder;
use Database\Seeders\Install\Paris\EstTerLigneSeeder;
use Database\Seeders\Install\ParisHubSeeder;
use Illuminate\Database\Seeder;

class InstallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $this->call(ParisHubSeeder::class);
        $this->call(EstTerLigneSeeder::class);

        $this->call(VersionSeeder::class);

        $this->call(BadgeSeeder::class);
    }
}
