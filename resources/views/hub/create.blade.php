@extends("template")

@section("css")

@endsection

@section("bread")
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('home') }}" class="text-white">
                    <i class="ki-outline ki-home text-white fs-3"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Achat d'un HUB</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <form id="form_checkout_hub" action="{{ route('hub.store') }}" method="post">
        @csrf
        <div class="row mb-10">
            <div class="col-md-8 col-sm-12">
                <div class="card shadow-lg rounded-1 bg-gray-400 mb-10">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fa-solid fa-caret-right text-white me-3 fs-2"></i> Achat d'un
                            hub</h3>
                    </div>
                    <div class="card-body">
                        <div class="mb-10">
                            <label for="hub_id" class="form-label required">
                                Ligne disponible
                            </label>
                            <select id="hub_id" class="form-control" data-control="select2" name="hub_id"
                                    data-live-search="true" data-placeholder="Hub disponible ({{ $hubs->count() }})"
                                    required>
                                <option value=""></option>
                                @foreach($hubs as $hub)
                                    <option value="{{ $hub->id }}">{{ $hub->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card shadow-lg rounded-1 bg-gray-400 mb-10 d-none" data-card="mapEmplacement">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fa-solid fa-caret-right text-white me-3 fs-2"></i> Emplacement</h3>
                    </div>
                    <div class="card-body h-350px" id="mapEmplacementHub"></div>
                </div>
                <div class="row">
                    <div class="col-md-4 d-none" data-card="info">
                        <div class="fs-1 fw-bolder mb-2">Informations</div>
                        <div class="d-flex flex-column mb-1">
                            <div class="fw-bold">Région</div>
                            <span data-name="region">Pays de la Loire</span>
                        </div>
                        <div class="d-flex flex-column mb-1">
                            <div class="fw-bold">Pays</div>
                            <span data-name="pays">France</span>
                        </div>
                    </div>
                    <div class="col-md-4 d-none" data-card="infra">
                        <div class="fs-1 fw-bolder mb-2">Infrastructure</div>
                        <div class="d-flex flex-column mb-1">
                            <div class="fw-bold">Nombre de Quai</div>
                            <span data-name="nb_quai">Pays de la Loire</span>
                        </div>
                        <div class="d-flex flex-column mb-1">
                            <div class="fw-bold">Longueur des quais</div>
                            <span data-name="long_quai">France</span>
                        </div>
                        <div class="d-flex flex-column mb-1">
                            <table class="table table-rounded table-striped border border-2 border-gray-900 gy-4 gs-4">
                                <thead>
                                    <tr>
                                        <th>Commerce</th>
                                        <th>Publicité</th>
                                        <th>Parking</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-check="commerce"><i class="fa-solid fa-check-circle text-success fs-2"></i></td>
                                        <td data-check="pub"><i class="fa-solid fa-check-circle text-success fs-2"></i></td>
                                        <td data-check="parking"><i class="fa-solid fa-check-circle text-success fs-2"></i></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-4 d-none" data-card="donnees">
                        <div class="fs-1 fw-bolder mb-2">Données</div>
                        <table class="table table-rounded table-striped border border-2 border-gray-900 gy-4 gs-4">
                            <tbody>
                                <tr>
                                    <td>Nombre de commerce</td>
                                    <td data-cell="nb_commerce">25</td>
                                </tr>
                                <tr>
                                    <td>Nombre d'espace publicitaire</td>
                                    <td data-cell="nb_pub">25</td>
                                </tr>
                                <tr>
                                    <td>Nombre de place de parking</td>
                                    <td data-cell="nb_parking">25</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card shadow-lg rounded-1 bg-gray-400" id="cardRecap">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fa-solid fa-caret-right text-white me-3 fs-2"></i>
                            Récapitulatif</h3>
                    </div>
                    <div class="card-body">
                        <div id="recapInfo"></div>
                        <div id="possessesLigne"></div>
                        <div class="d-flex flex-row justify-content-between mb-3">
                            <div class="d-flex flex-column fs-1">
                                <strong>Hub:</strong>
                                <span data-name="hubName"></span>
                            </div>
                        </div>
                        <div class="fw-bolder fs-2 mb-2">Engins disponible</div>
                        <div class="d-flex flex-row justify-content-between mb-3">
                            <div class="symbol symbol-50px symbol-2by3" data-name="intercity">
                            </div>
                            <div class="symbol symbol-50px symbol-2by3" data-name="ter">
                            </div>
                            <div class="symbol symbol-50px symbol-2by3" data-name="tgv">
                            </div>
                        </div>
                        <div class="fw-bolder fs-2 mb-2">Afflux Voyageurs (niv 1)</div>
                        <table class="table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">Première classe</th>
                                <th class="text-center">Seconde Classe</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center" data-name="firstClass"></td>
                                <td class="text-center" data-name="secondClass"></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="fw-bolder fs-2">Prix</div>
                        <div class="d-flex flex-row justify-content-between mb-3">
                            <div class="d-flex flex-column">
                                <strong>Prix Brut</strong>
                                <span data-name="price">0,00 €</span>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between mb-3">
                            <div class="d-flex flex-column">
                                <strong>Subvention (<span data-name="percentSubvention">0</span>)</strong>
                                <span data-name="priceSubvention">0,00 €</span>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-center mb-3">
                            <div
                                class="d-flex flex-column justify-content-center border border-2 border-danger p-5 rounded-1 fs-1">
                                <strong>Prix à payer</strong>
                                <span data-name="netPrice">0,00 €</span>
                            </div>
                        </div>
                        <input type="hidden" name="price_subvention" value="">
                        <input type="hidden" name="price_net" value="">
                        <div class="d-flex flex-end">
                            <button type="submit" id="btnHubCheckout" class="btn btn-success" disabled><i
                                    class="fa-solid fa-euro-sign me-3"></i> Acheter ce hub
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script>
        let blockUiRecap = new KTBlockUI(document.querySelector('#cardRecap'), {
            message: '<div class="blockui-message"><span class="spinner-border text-primary"></span> Chargement...</div>',
        })

        $("#hub_id").on('change', e => {
            blockUiRecap.block()
            e.preventDefault()
            $.ajax({
                url: '/api/core/hub/info',
                data: {"hub_id": e.target.value, "user_id": {{ auth()->user()->id }}},
                success: data => {
                    blockUiRecap.release()
                    document.querySelector('[data-name="hubName"]').innerHTML = data.data.hub.name
                    document.querySelector('[data-name="ter"]').innerHTML = ""
                    document.querySelector('[data-name="tgv"]').innerHTML = ""
                    document.querySelector('[data-name="intercity"]').innerHTML = ""
                    data.data.logo_ter ? document.querySelector('[data-name="ter"]').innerHTML = `<img src="${data.data.logo_ter}" alt=""/>` : null
                    data.data.logo_tgv ? document.querySelector('[data-name="tgv"]').innerHTML = `<img src="${data.data.logo_tgv}" alt=""/>` : null
                    data.data.logo_intercite ? document.querySelector('[data-name="intercity"]').innerHTML = `<img src="${data.data.logo_intercite}" alt=""/>` : null
                    document.querySelector('[data-name="firstClass"]').innerHTML = `${data.data.hub.request_first_class} / par heure`
                    document.querySelector('[data-name="secondClass"]').innerHTML = `${data.data.hub.request_second_class} / par heure`
                    document.querySelector('[data-name="price"]').innerHTML = `${data.data.prix_brut}`
                    document.querySelector('[data-name="percentSubvention"]').innerHTML = `${data.data.subvention_percent}`
                    document.querySelector('[data-name="priceSubvention"]').innerHTML = `${data.data.subvention}`
                    document.querySelector('[data-name="netPrice"]').innerHTML = `${data.data.restant}`
                    document.querySelector('[name="price_subvention"]').value = data.data.subvention
                    document.querySelector('[name="price_net"]').value = data.data.restant
                    loadInfo(data.data)

                    if (data.data.dispo === false) {
                        document.querySelector("#btnHubCheckout").setAttribute('disabled', true)
                        document.querySelector('#recapInfo').innerHTML = `
                        <div class="alert alert-dismissible bg-light-danger d-flex flex-column flex-sm-row p-5 mb-10">
                            <i class="ki-duotone ki-notification-bing fs-2hx text-danger me-4 mb-5 mb-sm-0">
                                <span class="path1"></span><span class="path2"></span><span class="path3"></span>
                            </i>
                            <div class="d-flex flex-column pe-0 pe-sm-10">
                                <h4 class="fw-semibold">Erreur</h4>
                                <span>Votre solde ne permet pas l'achat de ce HUB</span>
                            </div>

                            <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                                <i class="ki-duotone ki-cross fs-1 text-primary"><span class="path1"></span><span class="path2"></span></i>
                            </button>
                        </div>
                        `
                    } else {
                        document.querySelector("#btnHubCheckout").removeAttribute('disabled')
                        document.querySelector('#recapInfo').innerHTML = ``
                    }

                    if(data.data.possess === true) {
                        document.querySelector("#btnHubCheckout").setAttribute('disabled', true)
                        document.querySelector('#possessesLigne').innerHTML = `
                        <div class="alert alert-dismissible bg-light-danger d-flex flex-column flex-sm-row p-5 mb-10">
                            <i class="ki-duotone ki-notification-bing fs-2hx text-danger me-4 mb-5 mb-sm-0">
                                <span class="path1"></span><span class="path2"></span><span class="path3"></span>
                            </i>
                            <div class="d-flex flex-column pe-0 pe-sm-10">
                                <h4 class="fw-semibold">Erreur</h4>
                                <span>Vous possédez déjà ce HUB.</span>
                            </div>

                            <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                                <i class="ki-duotone ki-cross fs-1 text-primary"><span class="path1"></span><span class="path2"></span></i>
                            </button>
                        </div>
                        `
                    } else {
                        document.querySelector("#btnHubCheckout").removeAttribute('disabled')
                        document.querySelector('#possessesLigne').innerHTML = ``
                    }
                }
            })
        })

        function loadInfo(information) {
            document.querySelector('[data-card="info"]').classList.remove('d-none')
            document.querySelector('[data-name="region"]').innerHTML = information.hub.region
            document.querySelector('[data-name="pays"]').innerHTML = information.hub.pays

            document.querySelector('[data-card="infra"]').classList.remove('d-none')
            document.querySelector('[data-name="nb_quai"]').innerHTML = information.hub.nb_quai
            document.querySelector('[data-name="long_quai"]').innerHTML = information.hub.long_quai+" m"

            information.hub.commerce === 1 ?
                document.querySelector('[data-check="commerce"]').innerHTML = `<i class="fa-solid fa-check-circle text-success fs-2"></i>` :
                `<i class="fa-solid fa-xmark-circle text-danger fs-2"></i>`

            information.hub.publicite === 1 ?
                document.querySelector('[data-check="pub"]').innerHTML = `<i class="fa-solid fa-check-circle text-success fs-2"></i>` :
                `<i class="fa-solid fa-xmark-circle text-danger fs-2"></i>`

            information.hub.parking === 1 ?
                document.querySelector('[data-check="parking"]').innerHTML = `<i class="fa-solid fa-check-circle text-success fs-2"></i>` :
                `<i class="fa-solid fa-xmark-circle text-danger fs-2"></i>`

            document.querySelector('[data-card="donnees"]').classList.remove('d-none')
            document.querySelector('[data-cell="nb_commerce"]').innerHTML = information.hub.nb_slot_commerce
            document.querySelector('[data-cell="nb_pub"]').innerHTML = information.hub.nb_slot_publicite
            document.querySelector('[data-cell="nb_parking"]').innerHTML = information.hub.nb_slot_parking

        }
    </script>
@endsection
