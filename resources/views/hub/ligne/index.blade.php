@extends("template")

@section("css")

@endsection

@section("bread")
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('home') }}" class="text-white">
                    <i class="ki-outline ki-home text-white fs-3"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Mes Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.show', $hub->id) }}" class="text-white">
                    {{ $hub->hub->name }}
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Lignes</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("name_hub")
    <div id="kt_app_toolbar" class="app-toolbar py-6" style="background: url('/storages/wallpaper/hubs/{{ Str::snake(Str::lower($hub->hub->name)) }}.jpg') no-repeat center;background-size: cover;">
@endsection

@section("content")
    <div class="app-content-menu menu menu-rounded menu-gray-800 menu-state-bg flex-wrap fs-5 fw-semibold border-bottom mt-n7 pt-5 px-10 mb-10">
        <!--begin::Menu item-->
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success " href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Aperçu</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success active" href="{{ route('hub.ligne.index', $hub->id) }}">
                <span class="menu-title text-gray-800">Lignes</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Technicentre</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Commerces</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Publicités</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Parking</span>
            </a>
        </div>
        <!--end::Menu item-->
    </div>

    <div class="d-flex flex-end mb-5 mt-0">
        <a href="{{ route("hub.ligne.create", $hub->id) }}" class="btn btn-outline btn-outline-primary"><i class="fa-solid fa-euro-sign"></i> Acheter une ligne</a>
    </div>


    @if(count($lignes) != 0)
        @foreach($lignes as $ligne)
            <a class="d-flex flex-row shadow-lg rounded-3 p-5 text-black mb-10" href="{{ route('hub.ligne.show', [$hub->id, $ligne->id]) }}">
                <div class="d-flex flex-column justify-content-between">
                    <div class="d-flex flex-row justify-content-between mb-3">
                        <div class="d-flex flex-row">
                            <span class="badge badge-sm badge-primary me-5">LIGNE</span>
                            <div class="fw-bold">{{ $ligne->ligne->station_start }} <=> >{{ $ligne->ligne->station_end }}</div>
                        </div>
                    </div>
                </div>
            </a>
        @endforeach
    @else
        <div class="d-flex flex-row justify-content-center shadow-lg rounded-3 p-5 text-danger mb-10">
            <i class="ki-duotone ki-information fs-1"></i> Aucune ligne actuellement disponible dans ce HUB, veuillez en acheter une !
        </div>
    @endif

@endsection

@section("script")

@endsection
