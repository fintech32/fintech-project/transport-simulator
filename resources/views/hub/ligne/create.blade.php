@extends("template")

@section("css")

@endsection

@section("bread")
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('home') }}" class="text-white">
                    <i class="ki-outline ki-home text-white fs-3"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Mes Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.show', $hub->id) }}" class="text-white">
                    {{ $hub->hub->name }}
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.ligne.index', $hub->id) }}" class="text-white">
                    Lignes
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Achat d'une nouvelle ligne</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("name_hub")
    <div id="kt_app_toolbar" class="app-toolbar py-6" style="background: url('/storages/wallpaper/hubs/{{ Str::snake(Str::lower($hub->hub->name)) }}.jpg') no-repeat center;background-size: cover;">
@endsection

@section("content")
    <div class="app-content-menu menu menu-rounded menu-gray-800 menu-state-bg flex-wrap fs-5 fw-semibold border-bottom mt-n7 pt-5 px-10 mb-10">
        <!--begin::Menu item-->
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success " href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Aperçu</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success active" href="{{ route('hub.ligne.index', $hub->id) }}">
                <span class="menu-title text-gray-800">Lignes</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Technicentre</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Commerces</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Publicités</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Parking</span>
            </a>
        </div>
        <!--end::Menu item-->
    </div>
    <form id="form_adding_ligne" action="{{ route('hub.ligne.store', $hub->id) }}" method="post">
        @csrf
        <div class="row mb-10">
            <div class="col-md-8 col-sm-12">
                <div class="card shadow-lg rounded-1 bg-gray-400">
                    <div class="card-header">
                        <h3 class="card-title text-white fw-bold">Achat d'une nouvelle ligne</h3>
                    </div>
                    <div class="card-body">
                        <div class="mb-10">
                            <label for="ligne_id" class="form-label required">
                                Ligne disponible
                            </label>
                            <select id="ligne_id" class="form-control" data-control="select2" name="ligne_id" data-live-search="true" data-placeholder="Ligne disponible" required>
                                <option value=""></option>
                                @foreach($lignes as $ligne)
                                    <option value="{{ $ligne->id }}" data-content="{{ $ligne->station_start }} / {{ $ligne->station_end }}">{{ $ligne->station_start }} / {{ $ligne->station_end }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="card shadow-lg rounded-1 bg-gray-400">
                    <div class="card-header">
                        <h3 class="card-title text-white fw-bold">Récapitulatif</h3>
                    </div>
                    <div class="card-body">
                        <div id="recapInfo">
                        </div>
                        <div class="d-flex flex-row justify-content-between mb-3">
                            <div class="d-flex flex-column fs-1">
                                <strong>Ligne:</strong>
                                <span data-name="stationLine">Le Mans / Caen</span>
                            </div>
                        </div>
                        <div class="fw-bolder fs-2">Informations</div>
                        <div class="d-flex flex-row justify-content-between mb-3">
                            <div class="d-flex flex-column">
                                <strong>Nombre d'arret</strong>
                                <span data-name="stationPointCount">Le Mans / Caen</span>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between mb-3">
                            <div class="d-flex flex-column">
                                <strong>Distance</strong>
                                <span data-name="stationDistance">Le Mans / Caen</span>
                            </div>
                        </div>
                        <div class="d-flex flex-row justify-content-between mb-3">
                            <div class="d-flex flex-column">
                                <strong>Temps de trajets</strong>
                                <span data-name="stationTravelTime">Le Mans / Caen</span>
                            </div>
                        </div>
                        <div class="fw-bolder fs-2">Prix</div>

                        <div class="d-flex flex-row justify-content-center mb-3">
                            <div class="d-flex flex-column justify-content-center border border-2 border-danger p-5 rounded-1 fs-1">
                                <strong>Prix à payer</strong>
                                <span data-name="netPrice">Le Mans / Caen</span>
                            </div>
                        </div>
                        <div class="d-flex flex-end">
                            <button id="btnLigneCheckout" class="btn btn-success" disabled><i class="fa-solid fa-check-circle"></i> Acheter la ligne</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

@endsection

@section("script")
    <script type="text/javascript">
        $("#ligne_id").on('change', e => {
            e.preventDefault()
            $.ajax({
                url: '/api/core/ligne/info/'+e.target.value,
                data: {"user_hub_id": '{{ $hub->id }}'},
                success: data => {
                    document.querySelector('[data-name="stationLine"]').innerHTML = `${data.data.ligne.station_start} <=>  ${data.data.ligne.station_end}`
                    document.querySelector('[data-name="stationPointCount"]').innerHTML = `${data.data.ligne.nb_station}`
                    document.querySelector('[data-name="stationDistance"]').innerHTML = `${data.data.ligne.distance} Km`
                    document.querySelector('[data-name="stationTravelTime"]').innerHTML = `${data.data.ligne.time_min} Min`
                    document.querySelector('[data-name="netPrice"]').innerHTML = data.data.prix_brut

                    if(data.data.error_ligne_acl) {
                        document.querySelector('#btnLigneCheckout').setAttribute('disabled', true)
                        document.querySelector('#recapInfo').innerHTML = `
                        <div class="alert alert-dismissible bg-light-danger d-flex flex-column flex-sm-row p-5 mb-10">
                            <i class="ki-duotone ki-notification-bing fs-2hx text-danger me-4 mb-5 mb-sm-0">
                                <span class="path1"></span><span class="path2"></span><span class="path3"></span>
                            </i>
                            <div class="d-flex flex-column pe-0 pe-sm-10">
                                <h4 class="fw-semibold">Erreur</h4>
                                <span>Cette ligne vous appartient déjà !</span>
                            </div>

                            <button type="button" class="position-absolute position-sm-relative m-2 m-sm-0 top-0 end-0 btn btn-icon ms-sm-auto" data-bs-dismiss="alert">
                                <i class="ki-duotone ki-cross fs-1 text-primary"><span class="path1"></span><span class="path2"></span></i>
                            </button>
                        </div>
                        `
                    } else {
                        document.querySelector('#btnLigneCheckout').removeAttribute('disabled')
                        document.querySelector('#recapInfo').innerHTML = ``
                    }
                }
            })
        })
    </script>
@endsection
