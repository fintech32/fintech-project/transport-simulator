@extends("template")

@section("css")

@endsection

@section("bread")
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('home') }}" class="text-white">
                    <i class="ki-outline ki-home text-white fs-3"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Mes Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.show', $hub->id) }}" class="text-white">
                    {{ $hub->hub->name }}
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.ligne.index', $hub->id) }}" class="text-white">
                    Mes Lignes
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">{{ $ligne->ligne->station_start }} / {{ $ligne->ligne->station_end }}</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("name_hub")
    <div id="kt_app_toolbar" class="app-toolbar py-6" style="background: url('/storages/wallpaper/hubs/{{ Str::snake(Str::lower($hub->hub->name)) }}.jpg') no-repeat center;background-size: cover;">
@endsection

@section("content")
    <div class="app-content-menu menu menu-rounded menu-gray-800 menu-state-bg flex-wrap fs-5 fw-semibold border-bottom mt-n7 pt-5 pb-0 px-10 mb-10">
                <!--begin::Menu item-->
                <div class="menu-item pe-2">
                    <a class="menu-link px-5 text-active-success " href="{{ route('hub.show', $hub->id) }}">
                        <span class="menu-title text-gray-800">Aperçu</span>
                    </a>
                </div>
                <div class="menu-item pe-2">
                    <a class="menu-link px-5 text-active-success active" href="{{ route('hub.ligne.index', $hub->id) }}">
                        <span class="menu-title text-gray-800">Lignes</span>
                    </a>
                </div>
                <div class="menu-item pe-2">
                    <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                        <span class="menu-title text-gray-800">Technicentre</span>
                    </a>
                </div>
                <div class="menu-item pe-2">
                    <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                        <span class="menu-title text-gray-800">Commerces</span>
                    </a>
                </div>
                <div class="menu-item pe-2">
                    <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                        <span class="menu-title text-gray-800">Publicités</span>
                    </a>
                </div>
                <div class="menu-item pe-2">
                    <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                        <span class="menu-title text-gray-800">Parking</span>
                    </a>
                </div>
                <!--end::Menu item-->
            </div>

    <div class="d-flex justify-content-end">
        <ul class="nav nav-tabs nav-line-tabs mb-5 fs-6">
            <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" href="#details"><i class="ki-duotone ki-information-3"></i> Détail de la ligne</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#travel">Trajet sur la ligne</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" href="#sell">Vendre la ligne</a>
            </li>
        </ul>
    </div>

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="details" role="tabpanel">
            ...
        </div>
        <div class="tab-pane fade" id="travel" role="tabpanel">
            ...
        </div>
        <div class="tab-pane fade" id="sell" role="tabpanel">
            ...
        </div>
    </div>


@endsection

@section("script")

@endsection
