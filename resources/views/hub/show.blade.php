@extends("template")

@section("css")
    <link rel='stylesheet' href='https://unpkg.com/leaflet@1.8.0/dist/leaflet.css' crossorigin='' />
@endsection

@section("bread")
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('home') }}" class="text-white">
                    <i class="ki-outline ki-home text-white fs-3"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Mes Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">{{ $hub->hub->name }}</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("name_hub")
    <div id="kt_app_toolbar" class="app-toolbar py-6" style="background: url('/storages/wallpaper/hubs/{{ Str::snake(Str::lower($hub->hub->name)) }}.jpg') no-repeat center;background-size: cover;">
@endsection

@section("content")
    <div class="app-content-menu menu menu-rounded menu-gray-800 menu-state-bg flex-wrap fs-5 fw-semibold border-bottom mt-n7 pt-5 pb-6 px-10 mb-10">
        <!--begin::Menu item-->
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success active" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Aperçu</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.ligne.index', $hub->id) }}">
                <span class="menu-title text-gray-800">Lignes</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Technicentre</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Commerces</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Publicités</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Parking</span>
            </a>
        </div>
        <div class="menu-item pe-2">
            <a class="menu-link px-5 text-active-success" href="{{ route('hub.show', $hub->id) }}">
                <span class="menu-title text-gray-800">Recherches & Developpements</span>
            </a>
        </div>
        <!--end::Menu item-->
    </div>

    <div class="shadow-lg rounded-2 bg-gray-300 p-5 mb-10">
        <div id="map_hub" class="h-350px"></div>
    </div>

    @include("hub.eva_start")
    @include("hub.eva_end")

    <div class="shadow-lg rounded-2 bg-gray-300 p-5 mb-5">
        <div class="d-flex flex-row align-items-center fs-2 mb-3">
            <i class="ki-outline ki-information fs-2 text-black me-3"></i> Informations
        </div>
        <div class="d-flex flex-row">
            <div class="d-flex flex-row align-items-center mb-5">
                <span class="badge badge-warning me-3">HUB</span>
                {{ $hub->hub->name }} / {{ $hub->hub->region }} / <span class="iconify me-3 ms-2" data-icon="emojione-v1:flag-for-{{ Str::lower($hub->hub->pays) }}"></span> {{ $hub->hub->pays }}
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="d-flex flex-row justify-content-between mb-3">
                    <div class="me-5">Date d'Achat: </div>
                    <b>{{ $hub->date_achat->format('d/m/Y à H:i') }}</b>
                </div>
                <div class="d-flex flex-row justify-content-between mb-3">
                    <div class="me-5">Passager Annuel: </div>
                    <b>{{ number_format($hub->hub->count_annual_passengers, 0, null, " ") }}</b>
                </div>
                <div class="d-flex flex-row justify-content-between align-items-center mb-3">
                    <div class="me-5">Performance des lignes: </div>
                    <i class="ki-solid ki-arrow-up-right text-success fs-2tx"></i>
                </div>
            </div>
            <div class="col">
                <div class="d-flex flex-row justify-content-between mb-3">
                    <div class="me-5">Nombre de quai: </div>
                    <b>{{ $hub->hub->nb_quai }}</b>
                </div>
                <div class="d-flex flex-row justify-content-between mb-3">
                    <div class="me-5">Nombre de quai disponible: </div>
                    <b>{{ $hub->lignes->count() }} / {{ $hub->hub->nb_quai }}</b>
                </div>
                <div class="d-flex flex-row justify-content-between mb-3">
                    <div class="me-5">Kilomètre de ligne sur ce hub: </div>
                    <b>{{ $hub->km_ligne }} km</b>
                </div>
                <div class="d-flex flex-row justify-content-between mb-3">
                    <div class="me-5">Taxe d'exploitation: </div>
                    <b>{{ eur($hub->hub->taxe_hub) }}</b>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="shadow-lg rounded-2 bg-gray-300 p-5 mb-5">
                <div class="d-flex flex-row align-items-center fs-2 mb-3">
                    <i class="ki-outline ki-chart-simple fs-2 text-black me-3"></i> Statistiques
                </div>
                <div class="mb-5">
                    <div class="fw-bolder mb-3">Chiffre d'affaire</div>
                    <div class="d-flex flex-row justify-content-between mb-2">
                        <div>Aujourd'hui</div>
                        <div>{{ eur(\App\Trait\ComptaTrait::calcCAJournalier(now(), $hub->id)) }}</div>
                    </div>
                    <div class="d-flex flex-row justify-content-between mb-2">
                        <div>Hier</div>
                        <div>{{ eur(\App\Trait\ComptaTrait::calcCAJournalier(now()->subDay(), $hub->id)) }}</div>
                    </div>
                </div>
                <div class="mb-5">
                    <div class="fw-bolder mb-3">Bénéfice</div>
                    <div class="d-flex flex-row justify-content-between mb-2">
                        <div>Aujourd'hui</div>
                        <div>{{ eur(\App\Trait\ComptaTrait::calcBeneficeJournalier(now(), $hub->id)) }}</div>
                    </div>
                    <div class="d-flex flex-row justify-content-between mb-2">
                        <div>Hier</div>
                        <div>{{ eur(\App\Trait\ComptaTrait::calcBeneficeJournalier(now()->subDay(), $hub->id)) }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("script")
            <script src='https://unpkg.com/leaflet@1.8.0/dist/leaflet.js' crossorigin=''></script>
            <script type="text/javascript">
                function refresh(){
                    var t = 1000; // rafraîchissement en millisecondes
                    setTimeout('showDate()',t)
                }

                function showDate() {
                    var date = new Date()
                    var h = date.getHours();
                    var m = date.getMinutes();
                    var s = date.getSeconds();
                    if( h < 10 ){ h = '0' + h; }
                    if( m < 10 ){ m = '0' + m; }
                    if( s < 10 ){ s = '0' + s; }
                    var time = h + ':' + m + ':' + s
                    document.getElementById('horloge').innerHTML = time;
                    refresh();
                }
                let map,markers = [];
                function initMap() {
                    map = L.map('map_hub', {
                        center: {
                            lat: {{ $hub->hub->latitude }},
                            lng: {{ $hub->hub->longitude }},
                        },
                        zoom: 15
                    })

                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                        attribution: '© OpenStreetMap'
                    }).addTo(map)

                    map.on('click', mapClicked);
                }
                initMap()

                function mapClicked($event) {
                    console.log(map);
                    console.log($event.latlng.lat, $event.latlng.lng);
                }


            </script>
@endsection
