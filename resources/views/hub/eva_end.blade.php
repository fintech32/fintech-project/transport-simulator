<div class="table-responsive">
    <table class="table table-striped gy-7 gs-7">
        <thead>
        <tr class="eva-background">
            <th colspan="2" class="text-white">
                <i class="fa-solid fa-train eva-color-title fs-2 me-3"></i>
                <span class="fs-1 fw-bold">Arrivées</span>
            </th>
            <th class="p-5 d-flex flex-end">
                <div class="rounded-1 border border-2 border-primary my-2 me-3 w-50">
                    <div id="MyClockDisplay" class="clock px-2 fs-2 text-center" onload="showTime()"></div>
                </div>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr class="align-items-center">
            <td class="bg-gray-400 p-5">
                <span class="fw-bolder fs-2x">06:00</span>
                <div class="d-flex flex-row rounded-3 border border-1 border-warning bg-light-warning p-2 w-50 text-center">
                    <i class="ki-outline ki-time text-warning fs-2 me-2"></i> <span class="text-warning">Retard</span>
                </div>
            </td>
            <td class="p-5 d-flex flex-column">
                <div class="d-flex flex-row justify-content-between">
                    <div class="d-flex flex-column">
                        <span class="fw-bolder fs-2x">Nantes</span>
                        <div class="d-flex flex-row align-items-center">
                            <span class="text-primary me-5 fs-3">via</span>
                            <span class="fs-1">Le Mans - Angers-st-Laud</span>
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-center align-items-center eva-background w-75px h-75px text-white rounded-1">
                        <span class="fs-2">Voie</span>
                        <span class="fs-2x fw-bold">3</span>
                    </div>
                </div>
                <marquee direction="right" width="350">
                    <b>Ce texte défile de façon alternée</b>
                </marquee>
            </td>
            <td class="p-5 bg-primary">
                <div class="d-flex flex-column align-items-center justify-content-center">
                    <span class="text-black fs-4">TGV</span>
                    <span class="text-black fw-bold fs-3">8852</span>
                </div>
            </td>
        </tr>

        </tbody>
    </table>
</div>
