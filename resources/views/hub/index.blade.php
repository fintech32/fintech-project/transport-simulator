@extends("template")

@section("css")

@endsection

@section("bread")
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('home') }}" class="text-white">
                    <i class="ki-outline ki-home text-white fs-3"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('hub.index') }}" class="text-white">
                    Hubs
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Mes Hubs</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    @foreach($hubs as $hub)
        <a class="d-flex flex-row shadow-lg rounded-3 p-5 text-black" href="{{ route('hub.show', $hub->id) }}">
            <div class="d-flex flex-column justify-content-between">
                <div class="d-flex flex-row mb-3">
                    <span class="badge badge-sm badge-warning me-5">HUB</span>
                    <div class="fw-bold">{{ $hub->hub->name }} / {{ $hub->hub->region }} / {{ $hub->hub->pays }}</div>
                </div>
                <div class="d-flex flex-row">
                    <div class="me-3">Chiffre d'affaire: <b>{{ eur(\App\Trait\ComptaTrait::totalProduitForHub(auth()->user()->company, $hub->id)) }}</b></div>
                    <div class="me-3">Bénéfice: <b>{{ eur(\App\Trait\ComptaTrait::totalProduitForHub(auth()->user()->company, $hub->id) - \App\Trait\ComptaTrait::totalChargeForHub(auth()->user()->company, $hub->id)) }}</b></div>
                </div>
            </div>
        </a>
    @endforeach
@endsection

@section("script")

@endsection
