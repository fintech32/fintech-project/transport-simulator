@extends("template")

@section("css")

@endsection

@section("bread")
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('home') }}" class="text-white">
                    <i class="ki-outline ki-home text-white fs-3"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Bienvenue</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row p-5 mb-10 shadow-lg">
        <div class="symbol symbol-55px symbol-circle me-5">
            <img src="/assets/media/avatars/300-{{ rand(1,30) }}.jpg" alt="">
        </div>
        <div class="d-flex flex-column">
            <div class="fs-1 fw-bold">Bonjour {{ $user->name }} et bienvenue du {{ config('app.name') }} !</div>
            <p>
                Je me nomme {{ $faker->name() }} et je serai votre nouveau conseiller.<br>
                Vous êtes notre nouveau PDG en devenir de l'empire ferroviaire le plus ouvert sur le monde.<br>
                Mais avant de commencer à travailler, certaines informations son obligatoire avant de découvrir votre nouvelle
                structure.
            </p>
        </div>
    </div>

    <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card shadow-lg mb-5">
            <div class="card-header">
                <div class="card-title">Votre entreprise</div>
            </div>
            <div class="card-body">
                <x-form.input
                    name="name_company"
                    label="Nom de votre companie ferroviaire"
                    required="true" />
                <x-form.input-file
                    name="logo_company"
                    label="Logo de votre companie" />
            </div>
        </div>

        <div class="card shadow-lg mb-5">
            <div class="card-header">
                <div class="card-title">Secrétariat</div>
            </div>
            <div class="card-body">
                <x-form.input
                    name="name_secretary"
                    label="Nom de votre secrétaire"
                    required="true" />
                <x-form.input-file
                    name="avatar_secretary"
                    label="Avatar secretaire" />
            </div>
        </div>

        <div class="d-flex flex-end">
            <button class="btn btn-primary">Valider</button>
        </div>
    </form>
@endsection

@section("script")

@endsection
