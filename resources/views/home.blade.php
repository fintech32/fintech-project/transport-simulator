@extends("template")

@section("css")

@endsection

@section("bread")
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('home') }}" class="text-white">
                    <i class="ki-outline ki-home text-white fs-3"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Tableau de Bord</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="row mb-10">
        <div class="col">
            <div class="card shadow-sm bg-gray-300">
                <div class="card-header">
                    <h3 class="card-title">Votre Agenda</h3>
                </div>
                <div class="card-body">
                    <b>Réalisez les objectifs de l'Agenda du PDG en herbe et gagnez des ressources</b><br>
                    <!--begin::Accordion-->
                    <div class="accordion accordion-icon-collapse" id="kt_accordion_3">
                        @foreach(\App\Models\Core\Badge::limit(3)->get() as $badge)
                            <!--begin::Item-->
                            <div class="mb-5">
                                <!--begin::Header-->
                                <div class="accordion-header py-3 d-flex collapsed" data-bs-toggle="collapse" data-bs-target="#badge_{{ $badge->id }}">
                                    <span class="accordion-icon">
                                        <i class="ki-duotone ki-plus-square fs-3 accordion-icon-off"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                                        <i class="ki-duotone ki-minus-square fs-3 accordion-icon-on"><span class="path1"></span><span class="path2"></span></i>
                                    </span>
                                    <div class="d-flex flex-row justify-content-between">
                                        <h3 class="fs-4 fw-semibold mb-0 ms-4">{{ $badge->name }}</h3>
                                        @if(\App\Models\Users\UserBadge::where('user_id', auth()->user()->id)->where('badge_id', $badge->id)->count() == 1)
                                            <i class="ki-duotone ki-check text-success fs-2 text-end"></i>
                                        @endif
                                    </div>
                                </div>
                                <!--end::Header-->

                                <!--begin::Body-->
                                <div id="badge_{{ $badge->id }}" class="fs-6 collapse ps-10" data-bs-parent="#kt_accordion_3">
                                    <p class="fw-bold mb-2">{{ $badge->name }}</p>
                                    <div class="d-flex flex-row justify-content-between">
                                        Récompense:
                                        @foreach($badge->rewards as $reward)
                                            <div>
                                                {!! $reward->type_reward !!}
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Item-->
                        @endforeach
                    </div>
                    <!--end::Accordion-->
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card shadow-sm bg-gray-300">
                <div class="card-header">
                    <h3 class="card-title">Annonces & Actualités</h3>
                </div>
                <div class="card-body">
                    @foreach(\App\Models\Core\Annonce::where('status', true)->orderBy('created_at',"desc")->limit(5)->get() as $annonce)
                        <a class="d-flex flex-row align-items-center text-black bg-gray-400 p-3" href="">
                            <div class="fw-bolder me-5">{{ $annonce->updated_at->format('d/m/Y') }}</div>
                            <span class="d-flex flex-row">
                                <img src="/storages/wallpaper/news/{{ $annonce->img_news ?? 'placeholder.jpg' }}" alt="{{ $annonce->title }}">
                                <div class="d-flex flex-column mb-2 border-bottom-2">
                                    <div class="fw-bolder fs-base">{{ $annonce->title }}</div>
                                    <div class="fst-italic fs-6">{{ $annonce->synopsis }}</div>
                                </div>
                            </span>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-10">
        <div class="col-md-4">
            <div class="card shadow-sm bg-gray-300">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa-solid fa-building fs-2 me-2"></i> SITUATION DE VOTRE COMPAGNIE (M-1): {{ Str::ucfirst(auth()->user()->company->situation()->orderBy('date', 'desc')->first()->date->translatedFormat("F"))." ". auth()->user()->company->situation()->orderBy('date', 'desc')->first()->date->format("Y") }} </h3>
                </div>
                <div class="card-body">
                    <div class="d-flex flex-row justify-content-between pb-5">
                        <div>Chiffre d'affaire:</div>
                        <div class="">{{ eur(auth()->user()->company->situation()->orderBy('date', 'desc')->first()->turnover) }}</div>
                    </div>
                    <div class="d-flex flex-row justify-content-between pb-5">
                        <div>Coût des lignes:</div>
                        <div class="">{{ eur(auth()->user()->company->situation()->orderBy('date', 'desc')->first()->coast_travel) }}</div>
                    </div>
                    <div class="d-flex flex-row justify-content-between pb-5">
                        <div>Remboursement des emprunts:</div>
                        <div class="">{{ eur(auth()->user()->company->situation()->orderBy('date', 'desc')->first()->remb_emprunt) }}</div>
                    </div>
                    <div class="d-flex flex-row justify-content-between pb-5">
                        <div>Coût des locations:</div>
                        <div class="">{{ eur(auth()->user()->company->situation()->orderBy('date', 'desc')->first()->coast_location) }}</div>
                    </div>
                    <div class="d-flex flex-row justify-content-between pb-5">
                        <div>Trésorerie Structurelle:</div>
                        <div class="">{{ eur(auth()->user()->company->situation()->orderBy('date', 'desc')->first()->structural_cash) }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow-sm bg-gray-300 w-100">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa-solid fa-euro fs-2 me-2"></i> ÉVOLUTION DE VOS RÉSULTATS </h3>
                </div>
                <div class="card-body mh-250px">
                    {!! $chartEvolResultat->container() !!}
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card shadow-sm bg-gray-300">
                <div class="card-header">
                    <h3 class="card-title"><i class="fa-solid fa-book-open fs-2 me-2"></i> ÉVOLUTION DES INCIDENTS</h3>
                </div>
                <div class="card-body"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="d-flex flex-center align-items-center h-350px rounded-2 border border-primary shadow-lg">
                @include("hub.eva_start")
            </div>
        </div>
        <div class="col">
            <div class="d-flex flex-center align-items-center h-350px rounded-2 border border-success shadow-lg">
                @include("hub.eva_end")
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    {!! $chartEvolResultat->script() !!}
@endsection
