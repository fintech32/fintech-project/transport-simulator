<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic
Product Version: 8.1.8
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="{{ config('app.locale') }}">
<!--begin::Head-->
<head><base href=""/>
    <title>{{ config('app.name') }}</title>
    <meta charset="utf-8" />
    <meta name="theme-color" content="#217AFF"/>
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--begin::Fonts(mandatory for all pages)-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
    <!--end::Fonts-->
    @yield("css")
    <!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
    <link href="/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/assets/css/custom.css">
    <!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_app_body" data-kt-app-header-fixed-mobile="true" data-kt-app-toolbar-enabled="true" class="app-default">
<!--begin::Theme mode setup on page load-->
<script>
    var defaultThemeMode = "light";
    var themeMode;
    if ( document.documentElement ) {
        if ( document.documentElement.hasAttribute("data-bs-theme-mode")) {
            themeMode = document.documentElement.getAttribute("data-bs-theme-mode");
        } else {
            if ( localStorage.getItem("data-bs-theme") !== null ) {
                themeMode = localStorage.getItem("data-bs-theme");
            } else {
                themeMode = defaultThemeMode;
            }
        }

        if (themeMode === "system") {
            themeMode = window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
        }
        document.documentElement.setAttribute("data-bs-theme", themeMode);
    }
</script>
<!--end::Theme mode setup on page load-->
<!--begin::App-->
<div class="d-flex flex-column flex-root app-root" id="kt_app_root">
    <!--begin::Page-->
    <div class="app-page flex-column flex-column-fluid" id="kt_app_page">
        @include("includes.header")
        <!--begin::Wrapper-->
        <div class="app-wrapper flex-column flex-row-fluid" id="kt_app_wrapper">
            <!--begin::Toolbar-->
            @if(route_is(['hub.show', 'hub.ligne.*']))
                @yield("name_hub")
            @else
                <div id="kt_app_toolbar" class="app-toolbar py-6">
            @endif
                <!--begin::Toolbar container-->
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex align-items-start">
                    <!--begin::Toolbar container-->
                    <div class="d-flex flex-column flex-row-fluid">
                        @yield("bread")
                        <!--begin::Toolbar wrapper=-->
                        <div class="d-flex flex-stack flex-wrap flex-lg-nowrap gap-4 gap-lg-10 pt-6 pb-18 py-lg-13">
                            <!--begin::Page title-->
                            <div class="page-title d-flex align-items-center me-3">
                                <img alt="Logo" src="/storages/logos/company/{{ auth()->user()->logo_company }}" class="h-60px me-5" />
                                <!--begin::Title-->
                                <h1 class="page-heading d-flex text-white fw-bolder fs-2 flex-column justify-content-center my-0">{{ auth()->user()->name_company }}
                                    <!--begin::Description-->
                                    <span class="page-desc text-white opacity-50 fs-6 fw-bold pt-4">
                                        {{ auth()->user()->hubs()->get()->count() }} Gare Principal (Hub)
                                    </span>
                                    <!--end::Description--></h1>
                                <!--end::Title-->
                            </div>
                            <!--end::Page title-->
                            @if(auth()->user()->company)
                                <!--begin::Items-->
                                <div class="d-flex gap-4 gap-lg-13">
                                    <div class="d-flex flex-row align-items-center rounded-2 p-5">
                                        <div class="symbol symbol-30px me-3">
                                            <img style="" src="/storages/icons/euro.png" alt="Argent">
                                        </div>
                                        <div class="d-flex flex-column align-items-end">
                                            <!--begin::Number-->
                                            <span class="text-white fw-bold fs-3 mb-1" data-kt-countup="true" data-kt-countup-value="{{ auth()->user()->argent }}" data-kt-countup-suffix="€">0</span>
                                            <!--end::Number-->
                                            <!--begin::Section-->
                                            <div class="text-white opacity-50 fw-bold">Argent</div>
                                            <!--end::Section-->
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center  rounded-2 p-5">
                                        <div class="symbol symbol-30px me-3">
                                            <img style="" src="/storages/icons/tpoint.png" alt="T Point">
                                        </div>
                                        <div class="d-flex flex-column align-items-end">
                                            <!--begin::Number-->
                                            <span class="text-white fw-bold fs-3 mb-1" data-kt-countup="true" data-kt-countup-value="{{ auth()->user()->tpoint }}">0</span>
                                            <!--end::Number-->
                                            <!--begin::Section-->
                                            <div class="text-white opacity-50 fw-bold">T Point</div>
                                            <!--end::Section-->
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center  rounded-2 p-5">
                                        <div class="symbol symbol-30px me-3">
                                            <img style="" src="/storages/icons/research.png" alt="Point de Recherche">
                                        </div>
                                        <div class="d-flex flex-column align-items-end">
                                            <!--begin::Number-->
                                            <span class="text-white fw-bold fs-3 mb-1" data-kt-countup="true" data-kt-countup-value="{{ auth()->user()->research }}" data-kt-countup-suffix="€">0</span>
                                            <!--end::Number-->
                                            <!--begin::Section-->
                                            <div class="text-white opacity-50 fw-bold">Montant des recherches</div>
                                            <!--end::Section-->
                                        </div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center  rounded-2 p-5">
                                        <div class="d-flex flex-column align-items-end">
                                            <!--begin::Number-->
                                            <span class="text-white fw-bold fs-3 mb-1" data-kt-countup="true" data-kt-countup-value="{{ \App\Models\Users\UserCompany::calcSubvention(auth()->user()->company->treso_structure) }}" data-kt-countup-suffix="%">0</span>
                                            <!--end::Number-->
                                            <!--begin::Section-->
                                            <div class="text-white opacity-50 fw-bold">Subvention Mensuel</div>
                                            <!--end::Section-->
                                        </div>
                                    </div>
                                    @if(auth()->user()->hubs->count() != 0)
                                        <div class="d-flex flex-row align-items-center  rounded-2 p-5">
                                            <div class="d-flex flex-column align-items-end">
                                                <!--begin::Number-->
                                                <span class="text-white fw-bold fs-3 mb-1" data-kt-countup="true" data-kt-countup-value="{{ \App\Models\Users\UserCompany::calcImpot() }}" data-kt-countup-suffix="€">0</span>
                                                <!--end::Number-->
                                                <!--begin::Section-->
                                                <div class="text-white opacity-50 fw-bold">Montant du Prochain impot</div>
                                                <!--end::Section-->
                                            </div>
                                        </div>
                                    @endif
                                    <!--end::Item-->
                                </div>
                                <!--end::Items-->
                            @endauth
                        </div>
                        <!--end::Toolbar wrapper=-->
                    </div>
                    <!--end::Toolbar container=-->
                </div>
                <!--end::Toolbar container-->
            </div>
            <!--end::Toolbar-->
            <!--begin::Wrapper container-->
            <div class="app-container container-xxl">
                <!--begin::Main-->
                <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                    <!--begin::Content wrapper-->
                    <div class="d-flex flex-column flex-column-fluid">
                        <!--begin::Content-->
                        <div id="kt_app_content" class="app-content">
                            @include("includes.alert")
                           @yield("content")
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Content wrapper-->
                    <!--begin::Footer-->
                    <div id="kt_app_footer" class="app-footer d-flex flex-column flex-md-row align-items-center flex-center flex-md-stack py-2 py-lg-4">
                        <!--begin::Copyright-->
                        <div class="text-dark order-2 order-md-1">
                            <span class="text-muted fw-semibold me-1">2023&copy;</span>
                            <a href="https://keenthemes.com" target="_blank" class="text-gray-800 text-hover-primary">Keenthemes</a>
                        </div>
                        <!--end::Copyright-->
                        <!--begin::Menu-->
                        <ul class="menu menu-gray-600 menu-hover-primary fw-semibold order-1">
                            <li class="menu-item">
                                <a href="https://keenthemes.com" target="_blank" class="menu-link px-2">About</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://devs.keenthemes.com" target="_blank" class="menu-link px-2">Support</a>
                            </li>
                            <li class="menu-item">
                                <a href="https://1.envato.market/EA4JP" target="_blank" class="menu-link px-2">Purchase</a>
                            </li>
                        </ul>
                        <!--end::Menu-->
                    </div>
                    <!--end::Footer-->
                </div>
                <!--end:::Main-->
            </div>
            <!--end::Wrapper container-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
<!--end::App-->
<!--begin::Scrolltop-->
<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <i class="ki-outline ki-arrow-up"></i>
</div>
<!--end::Scrolltop-->
<!--begin::Javascript-->
<script>var hostUrl = "/assets/";</script>
<!--begin::Global Javascript Bundle(mandatory for all pages)-->
<script src="/assets/plugins/global/plugins.bundle.js"></script>
<script src="/assets/js/scripts.bundle.js"></script>
<!--end::Global Javascript Bundle-->
<script src="https://code.iconify.design/3/3.1.0/iconify.min.js"></script>
<script data-jsd-embedded data-key="176cc0ee-4340-4ca9-b66a-7a3cbdb401ea" data-base-url="https://jsd-widget.atlassian.com" src="https://jsd-widget.atlassian.com/assets/embed.js"></script>
<script src="{{ asset('/sw.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    window.addEventListener("load", () => {
        if ("serviceWorker" in navigator) {
            navigator.serviceWorker.register("/sw.js");
        }
    });

    function showTime(){
        var date = new Date();
        var h = date.getHours(); // 0 - 23
        var m = date.getMinutes(); // 0 - 59
        var s = date.getSeconds(); // 0 - 59
        var session = "AM";

        if(h == 0){
            h = 12;
        }

        if(h > 12){
            h = h - 12;
            session = "PM";
        }

        h = (h < 10) ? "0" + h : h;
        m = (m < 10) ? "0" + m : m;
        s = (s < 10) ? "0" + s : s;

        let time = `<span class="fw-bolder text-white fs-1 me-1">${h}:${m}</span><span class="fs-5 eva-color-text">${s}</span>`
        document.querySelector(".clock").innerHTML = time;
        document.querySelector(".clock").htmlContent = time;

        setTimeout(showTime, 1000);

    }

    showTime()
</script>
@yield("script")
<!--end::Javascript-->
</body>
<!--end::Body-->
</html>
