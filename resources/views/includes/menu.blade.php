<div class="app-header-menu app-header-mobile-drawer align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="app-header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="250px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_header_menu_toggle" data-kt-swapper="true" data-kt-swapper-mode="{default: 'append', lg: 'prepend'}" data-kt-swapper-parent="{default: '#kt_app_body', lg: '#kt_app_header_wrapper'}">
    <div class="menu menu-rounded menu-active-bg menu-state-primary menu-column menu-lg-row menu-title-gray-700 menu-icon-gray-500 menu-arrow-gray-500 menu-bullet-gray-500 my-5 my-lg-0 align-items-stretch fw-semibold px-2 px-lg-0" id="kt_app_header_menu" data-kt-menu="true">
        <div class="menu-item me-0 me-lg-2">
            <a href="{{ route('home') }}" class="menu-link active">
				<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
					<i class="ki-outline ki-element-11 text-primary fs-1"></i>
				</span>
                <span class="d-flex flex-column">
					<span class="fs-6 fw-bold text-gray-800">Tableau de Bord</span>
                    <!--<span class="fs-7 fw-semibold text-muted">Reports & statistics</span>-->
				</span>
            </a>
        </div>

        <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" data-kt-menu-offset="-50,0" class="menu-item menu-lg-down-accordion me-0 me-lg-2">
            <span href="{{ route('home') }}" class="menu-link">
				<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
					<!--<i class="ki-outline ki-element-11 text-primary fs-1"></i>-->
                    <img src="/storages/icons/railway_network.png" alt="" class="w-40px h-40px">
				</span>
                <span class="d-flex flex-column">
					<span class="fs-6 fw-bold text-gray-800">Gestion du réseau</span>
                    <!--<span class="fs-7 fw-semibold text-muted">Reports & statistics</span>-->
                    <span class="menu-arrow d-lg-none"></span>
				</span>
            </span>
            <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-200px">
                <div class="menu-item">
                    <a class="menu-link" href="{{ route('hub.index') }}">
						<span class="menu-icon">
                            <img src="/storages/icons/hub.png" alt="" class="w-20px h-20px">
						</span>
                        <span class="menu-title">Mes Hubs</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-link" href="{{ route('hub.create') }}">
						<span class="menu-icon">
							<i class="ki-outline ki-euro fs-2"></i>
						</span>
                        <span class="menu-title">Acheter un HUB</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
							<img src="/storages/icons/all_network.png" alt="" class="w-20px h-20px">
						</span>
                        <span class="menu-title">Mon Réseau</span>
                    </a>
                </div>
            </div>
        </div>
        <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" data-kt-menu-offset="-50,0" class="menu-item menu-lg-down-accordion me-0 me-lg-2">
            <span href="{{ route('home') }}" class="menu-link">
				<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
					<!--<i class="ki-outline ki-element-11 text-primary fs-1"></i>-->
                    <i class="ki-outline ki-home-3 text-danger fs-1"></i>
				</span>
                <span class="d-flex flex-column">
					<span class="fs-6 fw-bold text-gray-800">Gestion de la compagnie</span>
                    <!--<span class="fs-7 fw-semibold text-muted">Reports & statistics</span>-->
                    <span class="menu-arrow d-lg-none"></span>
				</span>
            </span>
            <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-200px">
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
                            <i class="ki-outline ki-colors-square fs-2"></i>
						</span>
                        <span class="menu-title">Personnalisation</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
                            <i class="ki-outline ki-profile-user fs-2"></i>
						</span>
                        <span class="menu-title">Profil</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
                            <i class="ki-outline ki-home-3 fs-2"></i>
						</span>
                        <span class="menu-title">Compagnie</span>
                    </a>
                </div>
            </div>
        </div>
        <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" data-kt-menu-offset="-50,0" class="menu-item menu-lg-down-accordion me-0 me-lg-2">
            <span href="{{ route('home') }}" class="menu-link">
				<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
					<!--<i class="ki-outline ki-element-11 text-primary fs-1"></i>-->
                    <i class="ki-outline ki-euro text-success fs-1"></i>
				</span>
                <span class="d-flex flex-column">
					<span class="fs-6 fw-bold text-gray-800">Finance</span>
                    <!--<span class="fs-7 fw-semibold text-muted">Reports & statistics</span>-->
                    <span class="menu-arrow d-lg-none"></span>
				</span>
            </span>
            <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-200px">
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
                            <i class="ki-outline ki-bank fs-2"></i>
						</span>
                        <span class="menu-title">Banque</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
                            <i class="ki-outline ki-chart-simple-2 fs-2"></i>
						</span>
                        <span class="menu-title">Comptabilité</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
                            <i class="ki-outline ki-euro fs-2"></i>
						</span>
                        <span class="menu-title">Finance</span>
                    </a>
                </div>
            </div>
        </div>
        <div data-kt-menu-trigger="{default: 'click', lg: 'hover'}" data-kt-menu-placement="bottom-start" data-kt-menu-offset="-50,0" class="menu-item menu-lg-down-accordion me-0 me-lg-2">
            <span href="{{ route('home') }}" class="menu-link">
				<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
					<!--<i class="ki-outline ki-element-11 text-primary fs-1"></i>-->
                    <i class="ki-outline ki-chart-line-up text-success fs-1"></i>
				</span>
                <span class="d-flex flex-column">
					<span class="fs-6 fw-bold text-gray-800">Concurrence</span>
                    <!--<span class="fs-7 fw-semibold text-muted">Reports & statistics</span>-->
                    <span class="menu-arrow d-lg-none"></span>
				</span>
            </span>
            <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown px-lg-2 py-lg-4 w-lg-200px">
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
                            <i class="ki-outline ki-ranking fs-2"></i>
						</span>
                        <span class="menu-title">Classement</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
                            <i class="ki-outline ki-parcel-tracking fs-2"></i>
						</span>
                        <span class="menu-title">Traffic</span>
                    </a>
                </div>
                <div class="menu-item">
                    <a class="menu-link" href="">
						<span class="menu-icon">
                            <i class="ki-outline ki-euro fs-2"></i>
						</span>
                        <span class="menu-title">Parts de marché</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="menu-item me-0 me-lg-2">
            <a href="{{ route('shop.index') }}" class="menu-link">
				<span class="menu-custom-icon d-flex flex-center flex-shrink-0 rounded w-40px h-40px me-3">
					<i class="ki-outline ki-handcart text-primary fs-1"></i>
				</span>
                <span class="d-flex flex-column">
					<span class="fs-6 fw-bold text-gray-800">Boutique</span>
                    <!--<span class="fs-7 fw-semibold text-muted">Compte Premium & T point</span>-->
				</span>
            </a>
        </div>
    </div>
</div>
