@extends("template")

@section("css")

@endsection

@section("bread")
    <!--begin::Toolbar wrapper-->
    <div class="d-flex align-items-center pt-1">
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-semibold">
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('home') }}" class="text-white">
                    <i class="ki-outline ki-home text-white fs-3"></i>
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">
                <a href="{{ route('shop.index') }}" class="text-white">
                    Boutique
                </a>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item">
                <i class="ki-outline ki-right fs-4 text-white mx-n1"></i>
            </li>
            <!--end::Item-->
            <!--begin::Item-->
            <li class="breadcrumb-item text-white fw-bold lh-1">Bienvenue</li>
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
    </div>
    <!--end::Toolbar wrapper=-->
@endsection

@section("content")
    <div class="d-flex flex-row">
        <div>
            <ul class="nav nav-tabs nav-pills flex-row border-0 flex-md-column me-5 mb-3 mb-md-0 fs-6 min-w-lg-200px">
                <li class="nav-item w-100 me-0 mb-md-2">
                    <a class="nav-link w-100 active btn btn-flex btn-active-light-success" data-bs-toggle="tab" href="#premium">
                        <span class="symbol symbol-25px me-3">
                            <img src="/storages/icons/premium.png" alt="Compte Premium">
                        </span>
                        <span class="d-flex flex-column align-items-start">
                            <span class="fs-4 fw-bold">Compte Premium</span>
                        </span>
                    </a>
                </li>
                <li class="nav-item w-100 me-0 mb-md-2">
                    <a class="nav-link w-100 btn btn-flex btn-active-light-info" data-bs-toggle="tab" href="#tpoint">
                        <span class="symbol symbol-25px me-3">
                            <img src="/storages/icons/tpoint.png" alt="Monnaie TPOINT">
                        </span>
                        <span class="d-flex flex-column align-items-start">
                            <span class="fs-4 fw-bold">T Point</span>
                        </span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="premium" role="tabpanel">
                <div class="d-flex flex-row">
                    <div data-kt-sticky="true"
                         data-kt-sticky-offset="{default: false, xl: '200px'}"
                         data-kt-sticky-width="{lg: '250px', xl: '300px'}"
                         data-kt-sticky-left="auto"
                         data-kt-sticky-top="380px"
                         data-kt-sticky-animation="false"
                         data-kt-sticky-zindex="95"
                         data-kt-sticky-name="premium">
                        @if(auth()->user()->premium)
                           <div class="d-flex flex-row align-items-center">
                               <i class="ki-solid ki-crown-2 fs-2x text-success me-3"></i>
                               <span class="fs-2 text-success">Votre compte est déjà premium</span>
                           </div>
                        @else
                            <stripe-buy-button
                                buy-button-id="buy_btn_1NXtYECAvfkAqiXO6UJaLbHB"
                                publishable-key="{{ config('services.stripe.public_key') }}"
                            >
                            </stripe-buy-button>
                        @endif
                    </div>
                    <div class="d-flex flex-column">
                        <div class="d-flex flex-row align-items-center p-5">
                            <span class="symbol symbol-100px symbol-2by3 me-3">
                                <img src="/storages/premium/carte_interactive.png" alt="Carte">
                            </span>
                            <div class="d-flex flex-column">
                                <div class="fs-2x">Carte Intéractive en temps réel</div>
                                <p class="fst-italic">Suivez vos trajets en direct sur la carte pour une meilleure immersion.</p>
                                <p class="fst-italic">Ainsi, chaque page disposant de la fonctionnalité vous affichera automatiquement le ou les engins en cours de trajet. Ces engins suivront leur trajectoire en temps réel. Immersion garantie !</p>
                            </div>
                        </div>
                        <div class="separator separator-dashed border-primary border-3 my-10"></div>
                        <div class="d-flex flex-row align-items-center p-5">
                            <span class="symbol symbol-100px me-3">
                                <img src="/storages/premium/gestion.png" alt="Carte">
                            </span>
                            <div class="d-flex flex-column">
                                <div class="fs-2x">Basculer entre comptabilité simplifiée et avancée</div>
                                <p class="fst-italic">Obtenez davantage d'informations sur les pages de gestion afin de faciliter vos décisions.</p>
                                <p class="fst-italic">Que ce soit sur la gestion des tarifs, l'historique de la demande, ou une facilité d'accès aux informations de l'audit Interne, la gestion devient plus simple grâce aux nouvelles données affichées.</p>
                            </div>
                        </div>
                        <div class="separator separator-dashed border-primary border-3 my-10"></div>
                        <div class="d-flex flex-row align-items-center p-5">
                            <span class="symbol symbol-100px me-3">
                                <img src="/storages/premium/gestion.png" alt="Carte">
                            </span>
                            <div class="d-flex flex-column">
                                <div class="fs-2x">Nouvelle Page d'acceuil</div>
                                <p class="fst-italic">À la connexion, ayez un regard sur l'ensemble de votre compagnie ferroviaire grâce à un nouveau tableau de bord sur la page d'accueil.</p>
                                <p class="fst-italic">4 nouveaux emplacements : la carte des trajets des lignes en direct sur la page d'accueil, le tableau des prochains départs façon Ecran EVA, les prochaines échéances des emprunts et un nouveau graphique de l'évolution de votre chiffre d'affaires.</p>
                            </div>
                        </div>
                        <div class="separator separator-dashed border-primary border-3 my-10"></div>
                        <div class="d-flex flex-row align-items-center p-5">
                            <span class="symbol symbol-100px symbol-2by3 me-3">
                                <img src="/storages/premium/ecran_eva.jpg" alt="Carte">
                            </span>
                            <div class="d-flex flex-column">
                                <div class="fs-2x">Affichage EVA</div>
                                <p class="fst-italic">Les nouveaux écrans infogare de la SNCF entre en scène pour les départs et les arrivées de vos HUBS</p>
                                <p class="fst-italic">
                                    Pour chaque Hub, 2 emplacements : le tableau des départs du hub et le tableau des arrivées en gare de votre hub.
                                </p>
                            </div>
                        </div>
                        <div class="separator separator-dashed border-primary border-3 my-10"></div>
                        <div class="d-flex flex-row align-items-center p-5">
                            <span class="symbol symbol-100px me-3">
                                <img src="/storages/premium/no_pub.png" alt="Carte">
                            </span>
                            <div class="d-flex flex-column">
                                <div class="fs-2x">Aucune publicité</div>
                                <p class="fst-italic">Fluidifier votre navigation en supprimant la publicité du jeu.</p>
                                <p class="fst-italic">
                                    Ainsi, tant que votre compte Premium sera actif, aucune publicité ne sera affichée. Parce que les bandeaux ne se chargent pas, votre navigation sur le jeu devient plus rapide.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tpoint" role="tabpanel">
                <div class="d-flex flex-row justify-content-around">
                    <div class="d-flex flex-column w-300px shadow-lg p-10 me-5">
                        <div class="symbol symbol-70px">
                            <img src="/storages/icons/tpoint.png" alt="">
                        </div>
                        <div class="fs-3 fw-bold">100 Tpoint</div>
                        <div class="d-flex flex-row justify-content-between">
                            <span class="fw-bold fs-1 text-success">0,99 €</span>
                            <a href="https://buy.stripe.com/test_8wMaFY1aEfez9wscMN" class="btn btn-sm btn-outline btn-outline-success">Payer</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

@section("script")
    <script async
            src="https://js.stripe.com/v3/buy-button.js">
    </script>
@endsection
