<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'contrib/php-fpm.php';
require 'contrib/npm.php';

set('application', 'Transport Simulator');
set('repository', 'https://gitlab.com/fintech32/fintech-project/transport-simulator.git');
set('php_fpm_version', '8.2');

host('prod')
    ->set('remote_user', 'debian')
    ->set('hostname', 'trs.trainznation.ovh')
    ->set('port', 999)
    ->set('deploy_path', '/www/wwwroot/{{hostname}}');

host('test')
    ->set('remote_user', 'debian')
    ->set('hostname', 'beta.trs.trainznation.ovh')
    ->set('port', 999)
    ->set('deploy_path', '/www/wwwroot/{{hostname}}');

task('deploy', [
    'deploy:prepare',
    'deploy:vendors',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'artisan:migrate',
    'npm:install',
    'npm:run:prod',
    'deploy:publish',
    'php-fpm:reload',
]);

task('npm:run:prod', function () {
    cd('{{release_or_current_path}}');
    run('npm run prod');
});

after('deploy:failed', 'deploy:unlock');
